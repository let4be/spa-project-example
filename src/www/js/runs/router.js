angular.module('NgApplication').run(($rootScope, router) => {
	$rootScope.isNotTheFirstState = router.isNotTheFirstState;
	$rootScope.showPopup = router.showPopup;
	$rootScope.hidePopup = router.hidePopup;
	$rootScope.isPopupState = router.isPopupState;
	$rootScope.isActiveState = router.isActiveState;
	$rootScope.isHomeState = router.isHomeState;
	$rootScope.isAddSchoolState = router.isAddSchoolState;
	$rootScope.isLoginState = router.isLoginState;
	$rootScope.previousStateName = router.previousStateName;
});