angular.module('NgApplication').config(($stateProvider, $urlRouterProvider, $locationProvider) => {
	$locationProvider.hashPrefix('!');
	$urlRouterProvider.otherwise("/search/schools");

	var states = {
		'search': {
			abstract: true,
			url: '/search',
			views: {
				'home-view': {
					templateUrl: 'pages/search.html',
					controller: 'SearchCtrl'
				},
				'fold-view': {
					templateUrl: 'pages/images.html'
				}
			}
		},

		'search.schools': {
			url: '/schools',
			views: {
				'images-view': {
					templateUrl: 'pages/images-albums.html'
				}
			}
		},

		'search.albums': {
			url: '/albums',
			views: {
				'images-view': {
					templateUrl: 'pages/images-albums.html'
				}
			}
		},

		'search.album': {
			url: '/album/:eventId',
			views: {
				'fold-view@': {
					templateUrl: 'pages/images-album.html'
				}
			}
		},

		'search.photos': {
			url: '/photos',
			views: {
				'images-view': {
					templateUrl: 'pages/images-photos.html'
				}
			}
		},

		'search.detail': {
			url: '/detail/:eventId/:photoName',
			views: {
				'fold-view@': {
					templateUrl: 'pages/images-details.html'
				}
			}
		},

		'contact': {
			url: '/contact',
			views: {
				'main-view@': {
					templateUrl: 'pages/contact.html'
				}
			}
		}
	};

	var popupStates = {
		'login': () => {
			return {
				url: '/login',
				views: {
					'popup-left': {
						templateUrl: 'pages/popup-login-left.html'
					},
					'popup-right': {
						templateUrl: 'pages/popup-login-right.html',
						controller: 'AuthCtrl'
					}
				}
			};
		},
		'add-school': () => {
			return {
				url: '/add-school',
				views: {
					'popup-left': {
						templateUrl: 'pages/popup-add-school-left.html'
					},
					'popup-right': {
						templateUrl: 'pages/popup-add-school-right.html'
					}
				}
			};
		},
		'no-school': () => {
			return {
				url: '/no-such-school/:name',
				views: {
					'popup-left': {
						templateUrl: 'pages/popup-no-school-left.html'
					},
					'popup-right': {
						templateUrl: 'pages/popup-no-school-right.html',
						controller: 'NoSchoolCtrl'
					}
				}
			};
		},
		'search-school': () => {
			return {
				url: '/search-school',
				views: {
					'popup-left': {
						templateUrl: 'pages/popup-search-school-left.html'
					},
					'popup-right': {
						templateUrl: 'pages/popup-search-school-right.html',
						controller: 'SearchCtrl'
					}
				}
			};
		}
	};

	var popupAbstractState = () => { return {
		abstract: true,
		data: {
			settings: {
				isLeftSide: true
			}
		},
		views: {
			'popup-view@': {
				templateUrl: 'pages/popup.html',
				controller: 'PopupCtrl'
			}
		}
	};};

	var state = (name, state) => {
		console.log('creating routing state: ', name, state);
		$stateProvider.state(name, state);
	};

	for(var stateName in states) {
		state(stateName, states[stateName]);
		state(stateName + '.popup', popupAbstractState());
		for (var popupStateName in popupStates)
			state(stateName + '.popup.' + popupStateName, popupStates[popupStateName]());
	}
});