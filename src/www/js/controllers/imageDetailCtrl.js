angular.module('NgApplication').controller('ImageDetailCtrl', function ($window, $scope, $state, $stateParams, $timeout, images, config, user, socket, app) {

	var loadFacebookSDK = (appId) => {
		console.log('loadFacebookSDK', appId);
		window.fbAsyncInit = function () {
			FB.init({
				appId: appId,
				status: true,
				cookie: true,
				xfbml: true,
				version: 'v2.0'
			});

			FB.XFBML.parse();
		};

		(function (d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) {
				return;
			}
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	};

	var calcIsShowBanner = () => $('#fold-view').height() + 200 <= $(window).height();
	$scope.isShowBanner = false;
	$scope.$watchCollection('[details, isDetailImageLoaded]', (v) => {
		if (v[0] && v[1])
			$timeout(() => {
				$scope.isShowBanner = calcIsShowBanner();
			});
	});
	$(window).resize(() => {
		$scope.isShowBanner = calcIsShowBanner();
		$scope.$apply();
	});

	function postToFacebook(url, callback) {
		console.log('postToFacebook', url);
		FB.ui({method: 'share', href: url}, callback);
	}

	var getSimilarPhotos = () => {
		if (images.list.length > 0) {
			$scope.similarPhotos = images.similarPhotos($scope.currentEventId, $scope.currentPhotoName, 3);
			$scope.nearestPhotos = images.nearestPhotos($scope.currentEventId, $scope.currentPhotoName, 5);
		}
	};

	var requestComments = () => {
		socket.emit('images/comments-request', {
			eventId: $scope.currentEventId,
			name: $scope.currentPhotoName
		}, (r) => {
			console.log('images/comments-request answer', r);

			if (r.comments)
				$scope.comments = r.comments;
		});
	};

	$scope.url = $window.location.href;
	$scope.image = new images.Photo({eventId: $stateParams.eventId, name: $stateParams.photoName});
	$scope.similarPhotos = [];
	$scope.nearestPhotos = [];

	$scope.isDetailImageLoaded = false;
	$scope.isImagesList = images.list.length > 0;
	$scope.tags = [];

	$scope.currentEventId = $stateParams.eventId;
	$scope.currentPhotoName = $stateParams.photoName;
	$scope.details = images.photoByKey($scope.currentEventId, $scope.currentPhotoName);
	$scope.comments = [];
	$scope.$watch('details', (d) => {
		$scope.tags = d ? [d.gender, d.sportLevel, d.sportType, d.home, d.visitor, d.date].filter(x => !!x) : [];

		if ($scope.details)
			$scope.title = `${$scope.details.gender} ${$scope.details.sportLevel} ${$scope.details.sportType}`;
	});

	$scope.photoDetailViewUrl = $window.location.protocol + '//' + $window.location.host + '/?social=1' + $state.href('search.detail', {eventId: $scope.currentEventId, photoName: $scope.currentPhotoName});
	$scope.prevPhoto = () => images.prevPhoto($scope.currentEventId, $scope.currentPhotoName);
	$scope.nextPhoto = () => images.nextPhoto($scope.currentEventId, $scope.currentPhotoName);

	$scope.viewState = images.viewState;

	var postAuth = [];

	var performActionWithAuth = (action) => {
		if (!user.isAuthenticated()) {
			postAuth.push(action);
			$scope.showPopup('login');
			return;
		}

		action();
	};

	$scope.vLike = () => {
		performActionWithAuth(() => {
			socket.emit('images/v-like', {
				eventId: $scope.currentEventId,
				name: $scope.currentPhotoName
			}, (r) => {
				console.log('images/v-like answer', r);

				if ('likes' in r)
					$scope.details.likesCounter = r.likes;
			});
		});
	};

	$scope.commentText = '';
	$scope.isAddCommentMode = false;
	$scope.isCommentSendMode = false;

	$scope.switchToAddCommentMode = () => {
		if (!user.isAuthenticated()) {
			$scope.showPopup('login', {}, () => {
				if (user.isAuthenticated()) {
					$scope.isAddCommentMode = !$scope.isAddCommentMode;
				}
			});
			return;
		}

		$scope.isAddCommentMode = !$scope.isAddCommentMode;
	};

	$scope.removeComment = (commentId) => {
		console.log('Remove comment request', commentId);

		socket.emit('images/remove-comment', {
			eventId: $scope.currentEventId,
			name: $scope.currentPhotoName,
			commentId: commentId
		}, (r) => {
			console.log('images/remove-сomment answer', r);
			if (r.isOk)
				$scope.comments = $scope.comments.filter(x => x.id != commentId);
		});
	};

	$scope.addComment = () => {
		console.log('Add comment request', $scope.commentText);

		$scope.isAddCommentMode = false;
		$scope.isCommentSendMode = true;

		if ($scope.commentText)
			$scope.commentText = $scope.commentText.trim().substr(0, 140);

		if ($scope.commentText)
			socket.emit('images/add-comment', {
				eventId: $scope.currentEventId,
				name: $scope.currentPhotoName,
				text: $scope.commentText
			}, (r) => {
				$scope.isCommentSendMode = false;

				if (r.isOk)
				{
					$scope.comments.unshift({
						id: r.commentId,
						text: $scope.commentText,
						date: 'just now',
						fullName: user.fullName,
						isMine: true
					});
					$scope.commentText = '';
				}
			});
		else
		{
			$scope.isCommentSendMode = false;
		}
	};

	$scope.imageRightClick = () => {

	};

	$scope.keyPressed = function(e) {
		if ($(':focus').length > 0)
			return;

		var p;
		if (e.which == 37) {
			p = $scope.prevPhoto();
			if (p.name)
				$state.go('search.detail', {eventId: p.eventId, photoName: p.name}, {reload: true});
		} else
		if (e.which == 39) {
			p = $scope.nextPhoto();
			if (p.name)
				$state.go('search.detail', {eventId: p.eventId, photoName: p.name}, {reload: true});
		}
	};

	$scope.shareFacebook = () => {
		postToFacebook(
			$scope.photoDetailViewUrl,
			(r) => {
				console.log('shareFacebook answer', r);
			}
		);
	};

	$scope.shareTwitter = () => {
		var text = $scope.title.toUpperCase() + '\n' + $scope.details.title + '\n';
		popupWindow(`//twitter.com/intent/tweet?url=${encodeURIComponent($scope.photoDetailViewUrl)}&text=${encodeURIComponent(text)}&hashtags=VarsityViews`, 'Share on Twitter', 800, 400);
	};

	$scope.shareGooglePlus = () => {
		popupWindow(`//plus.google.com/share?url=${encodeURIComponent($scope.photoDetailViewUrl)}`, 'Share on Google+', 800, 400);
	};

	$scope.sharePinterest = () => {
		popupWindow(`//www.pinterest.com/pin/create/button/?url=${encodeURIComponent($scope.photoDetailViewUrl)}&media=${encodeURIComponent($scope.image.detailUrl())}`, 'Share on Pinterest', 800, 400);
	};

	$scope.markImageAsInappropriate = () => {
		performActionWithAuth(() => {
			socket.emit('images/inappropriate-mark', {
				eventId: $scope.currentEventId,
				photoName: $scope.currentPhotoName
			}, (r) => {
				$scope.details.isInappropriateMark = r.isMarked;
			});
		});
	};

	$scope.onImageLoaded = () =>
	{
		$scope.isDetailImageLoaded = true;
	};

	$scope.$on('images/update', () => {
		$scope.isImagesList = images.list.length > 0;
		$scope.details = images.photoByKey($scope.currentEventId, $scope.currentPhotoName);
		console.log('$scope.details', $scope.details);

		if (user.isAuthenticated() && postAuth.length > 0) {
			for(var p in postAuth) {
				p = postAuth[p];
				((p) => {
					$timeout(() => {
						p();
					});
				})(p);
			}
			postAuth = [];
		}

		getSimilarPhotos();
	});


	getSimilarPhotos();
	requestComments();
	loadFacebookSDK(app.facebookAppId);
	$scope.initializeFixedBlocks();

	$scope.$on('hello', () => {
		requestComments();
	});
});

