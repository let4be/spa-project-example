console.log('Loading...');

var io = require('socket.io-client');

var socketConnection = io.connect('/', {
	transports: ['websocket', 'polling']
});

// neat stuff from the future!
require('traceur/bin/traceur-runtime');
require('es6-shim');

// nowhere without jquery...
window.jQuery = window.$ = require('jquery');

function getUrlParameter(sParam)
{
	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++)
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam)
		{
			return sParameterName[1];
		}
	}
	return false;
}

var fragment = unescape(getUrlParameter('_escaped_fragment_'));
console.log('fragment is', fragment);

if (fragment && fragment.startsWith('/')) {
	var loc = `${location.protocol}//${location.host}/#!${fragment}`;
	location.href = loc;
	return;
}

// angular
require('angular');
require('angular-ui-router');
require('angular-bootstrap');
require('angular-masonry');
require('angular-recaptcha');
require('angular-translate');
require('ngInfiniteScroll');
require('angulartics/src/angulartics.js');
require('angulartics/src/angulartics-ga.js');

var bulkRequire = require('bulk-require');

(function() {
	var initialized = false;
	var module = null;

	console.log('Connecting...');

	socketConnection.on('connected', (data) => {
		if (initialized) {
			module.value('app', data.app);
			return;
		}

		initialized = true;
		console.log('Initializing angular...', data);

		module = angular.module('NgApplication', ['ui.router', 'ui.bootstrap', 'infinite-scroll', 'angulartics', 'angulartics.google.analytics', 'vcRecaptcha', 'pascalprecht.translate']);
		module
			.config(($translateProvider, $uiViewScrollProvider, $anchorScrollProvider) => {
				$translateProvider.preferredLanguage('en');
				$uiViewScrollProvider.useAnchorScroll();
				$anchorScrollProvider.disableAutoScrolling();
			})
			.constant('socketConnection', socketConnection)
			.value('app', data.app);

		console.log('Loading angular templates...');
		$.getScript("js/templates.min.js", function(data, textStatus, jqxhr) {
			console.log('Loading angular code...');

			// require all in one
			bulkRequire(__dirname, [
				'runs/*.js',
				'configs/*.js',
				'services/*.js',
				'controllers/*.js',
				'directives/*.js',
				'helpers/*.js'
			]);

			console.log('Loading async css');
			loadCSS('//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css');

			console.log('Starting angular...');
			angular.element(document).ready(function() {
				console.log('Bootstrap angular now!');
				angular.bootstrap(document, ['NgApplication']);
			});
		});
	});
})();