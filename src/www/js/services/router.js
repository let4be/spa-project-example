angular.module('NgApplication').service('router', function ($rootScope, $state, $location) {
	var statesHistory = [];
	var parentStateName = $state.current.name;
	var stateNameWithoutPopup = (name) => name.replace(/\.popup\..+/, '');

	$rootScope.$on('$stateChangeSuccess', (event, toState, toParams, fromState, fromParams) => {
		statesHistory.push(toState.name);
		parentStateName = stateNameWithoutPopup(toState.name);
		if (!toState.name.endsWith(popup.stateName) && popup.onClose) {
			popup.onClose();
			popup = {};
		}
	});

	var popup = {
		stateName: '',
		onClose: null
	};

	this.showPopup = (stateName, params, onClose = null) => {
		stateName = `.popup.${stateName}`;
		popup.stateName = stateName;
		popup.onClose = onClose;

		if ($state.current.name.endsWith(stateName)) {
			$state.go($state.current.name.replace(stateName, ''));
		}
		else {
			$state.go(parentStateName + stateName, params);
		}
	};

	this.isFirstState = () => {
		console.log('isFirstState', statesHistory.length);
		return statesHistory.length < 2;
	};

	this.isNotTheFirstState = () => statesHistory.length > 1;

	this.hidePopup = () => {
		$state.go(stateNameWithoutPopup($state.current.name));
	};

	this.isPopupState = function (name) {
		return name.indexOf('.popup.') > 0;
	};

	this.previousStateName = () => statesHistory.length > 1 ? statesHistory.slice(-2)[0] : '';

	this.isActiveState = (name, isNested = false) => {
		return (name.startsWith('popup.') && $state.current.name.endsWith(name)) ||
			($state.current.name == name || $state.current.name.startsWith(`${name}.popup.`)) ||
			(isNested && $state.current.name.startsWith(name));
	};

	this.isHomeState = () => {
		return $state.current.name == 'search' || $state.current.name.startsWith('search.');
	};

	this.isAddSchoolState = () => {
		return $state.current.name.endsWith('popup.add-school');
	};

	this.isLoginState = () => {
		return $state.current.name.endsWith('popup.login');
	};
});