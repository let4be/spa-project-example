var classes = require('bl-classes');
var VWEvent = classes.VWEvent;

angular.module('NgApplication').service('images', function ($rootScope, $state, $stateParams, socket, router, config) {
	var that = this;

	var isInitialized = false;
	var isScrolling = false;

	var resetList = () => {
		that.list = [];
		that.listByKey = [];
		that.isFull = false;
		that.offset = 0;
		that.isMore = true;

		$rootScope.$broadcast('images/update');
	};

	var onFilterChanged = () =>
	{
		$rootScope.$broadcast('images/filters-set');

		socket.emit('images/filters-set', {
			filters: that.filters
		});
	};

	this.filters = null;
	this.listByKey = [];
	this.albums = [];
	this.list = [];

	this.isFull = false;
	this.offset = 0;
	this.isMore = true;

	var Photo = function (photo) {
		this.name = photo.name;
		this.eventId = photo.eventId;
		this.thumbUrl = () => config.AMAZON_S3_BASE_URL + '/' + photo.eventId + '/' + 'thumb' + '/' + photo.name;
		this.detailUrl = () => config.AMAZON_S3_BASE_URL + '/' + photo.eventId + '/' + 'detail' + '/' + photo.name;
		this.detail2Url = () => config.AMAZON_S3_BASE_URL + '/' + photo.eventId + '/' + 'detail-2' + '/' + photo.name;
		this.detail3Url = () => config.AMAZON_S3_BASE_URL + '/' + photo.eventId + '/' + 'detail-3' + '/' + photo.name;
	};

	this.Photo = Photo;

	var prevPhotoByIndex = (baseIndex, offset) => {
		var i = baseIndex - offset;
		if (i < 0)
			i = that.list.length + i;

		return new Photo({
			name: that.list[i].name,
			eventId: that.list[i].eventId
		});
	};

	var nextPhotoByIndex = (baseIndex, offset) => {
		var i = baseIndex + offset;
		if (i >= that.list.length)
			i = i - that.list.length;

		return new Photo({
			name: that.list[i].name,
			eventId: that.list[i].eventId
		});
	};

	this.prevPhoto = (eventId, name, index = 1) => {
		if (this.list.length < 1)
			return {};

		var i = this.list.findIndex(e => e.eventId == eventId && e.name == name);
		return prevPhotoByIndex(i, index);
	};

	this.nextPhoto = (eventId, name, index = 1) => {
		if (this.list.length < 1)
			return {};

		var i = this.list.findIndex(e => e.eventId == eventId && e.name == name);
		return nextPhotoByIndex(i, index);
	};

	this.nearestPhotos = (eventId, name, count = 3) => {
		if (this.list.length < 1)
			return {};

		var i = this.list.findIndex(e => e.eventId == eventId && e.name == name);

		var photos = [];

		for(var j = 1; j <= count; j++)
		{
			photos.push(prevPhotoByIndex(i, j));
			photos.push(nextPhotoByIndex(i, j));
		}

		return photos;
	};

	this.similarPhotos = (eventId, name, count) => {
		if (this.list.length < 1)
			return {};

		var i = this.list.findIndex(e => e.eventId == eventId && e.name == name);

		var photos = [];

		for(var j = 1; j <= count; j++)
			photos.push(nextPhotoByIndex(i, j));

		return photos;
	};

	this.photoByKey = (eventId, name) => that.listByKey[`${eventId}|${name}`];
	this.albumByEventId = (eventId) => that.albums[eventId];

	var Filter = function (opt) {
		this.name = opt.name ? opt.name : opt.longName;
		this.isSelected = opt.isSelected;

		this.id = opt.id ? opt.id : this.name;
	};

	var toggleCheck = (filters, filter) => {
		that.filters[filters] = that.filters[filters].map(x => {
				if (x.id == filter.id)
					x.isSelected = !x.isSelected;
				return x;
			}
		);
	};

	var uncheck = (filters, filter) => {
		that.filters[filters] = that.filters[filters].map(x => {
				if (x.id == filter.id)
					x.isSelected = false;
				return x;
			}
		);
	};

	this.toggleCheck = (filters, filter, def) => {
		toggleCheck(filters, filter);

		if (def)
		{
			var defIndex = that.filters[filters].findIndex(x => x.id == def && x.isSelected);
			if (defIndex > -1)
				if (filter.id == def)
					that.filters[filters] = that.filters[filters].map(x => {
						if (x.isSelected && x.id != def)
							x.isSelected = false;
						return x;
					});
				else
					toggleCheck(filters, {id: def});

			if (that.filters[filters].every(x => !x.isSelected))
				toggleCheck(filters, {id: def});
		}

		onFilterChanged();
	};

	this.toggleRadio = (filters, filter) => {
		that.filters[filters] = that.filters[filters].map(x => {
				x.isSelected = !x.isSelected;
				return x;
			}
		);

		onFilterChanged();
	};

	this.addSchool = (school) => {
		uncheck('schools', {id: 'NATIONAL'});
		school.isSelected = true;
		that.filters.schools.push(school);

		onFilterChanged();
	};

	this.removeSchool = (school) => {
		that.filters.schools = that.filters.schools.filter(x => x.id != school.id);

		if (that.filters.schools.every(x => !x.isSelected))
			toggleCheck('schools', {id: 'NATIONAL'});

		onFilterChanged();
	};


	this.infiniteScroll = () => {
		if (isScrolling || that.list.length < 1)
			return;

		isScrolling = true;

		socket.emit('images/scroll', {
			offset: that.list.length
		});
	};

	this.viewState = 'photos';

	var mapStateToFiltersFor = (state) => {
		if (state.name.contains('search.albums') || state.name.contains('search.schools'))
			return 'ALBUMS';

		if (state.name.contains('search.photos'))
			return 'PHOTOS';

		if (state.name.contains('search.album'))
			return 'ALBUM';

		if (state.name.contains('search.detail'))
			return 'DETAIL';

		return '';
	};

	var getStateFiltersRequirements = (state, stateParams) => {
		var r = {};
		r.kind = mapStateToFiltersFor(state);
		if (!r.kind)
			return {};

		if (stateParams.eventId)
			r.eventId = stateParams.eventId;

		console.log('getStateFiltersRequirements', r);

		return r;
	};

	this.initialize = () => {
		if (isInitialized)
			return;

		socket.on('images/set-filters', (data) => {
			console.log('images/set-filters', data);
			var currentStateFiltersFor = mapStateToFiltersFor($state.current);
			if (data.filters.for != currentStateFiltersFor) {
				console.log('images/set-filters declined due to different state local/remote', currentStateFiltersFor, data.filters.for);
				return;
			}

			that.filters = data.filters;

			$rootScope.$broadcast('images/filters-set');
		});

		var sort = 0;

		socket.on('images/update', (data) => {
			console.log('images/update', data);

			var currentStateFiltersFor = mapStateToFiltersFor($state.current);
			if (data.for != currentStateFiltersFor) {
				console.log('images/update declined due to different state local/remote', currentStateFiltersFor, data.for);
				return;
			}

			isScrolling = false;

			var list = data.list
				.map((e) => new VWEvent(e));

			that.isFull = data.isFull;
			if (data.isFull) {
				sort = 0;
				that.listByKey = [];
			}

			for (var e of list) {
				e.sort = sort++;
				that.listByKey[e.key] = e;
				if (!that.albums[e.eventId])
					that.albums[e.eventId] = e;
			}

			that.offset = data.offset;
			that.list = Object.keys(that.listByKey).map(key => that.listByKey[key]);
			that.isMore = data.isFull || list.length > 0;

			$rootScope.$broadcast('images/update');
		});

		var filtersRequest = (state, params) => {
			socket.emit('images/filters-request', {
				requirements: getStateFiltersRequirements(state, params)
			});
			$rootScope.$broadcast('images/loading');
		};

		var isAuthenticationStateKnown = false;

		$rootScope.$on('hey', () => {
			isAuthenticationStateKnown = true;
			filtersRequest($state.current, $state.params);
		});

		$rootScope.$on('hello', () => {
			isAuthenticationStateKnown = true;
			filtersRequest($state.current, $state.params);
		});

		$rootScope.$on('bye', () => {
			filtersRequest($state.current, $state.params);
		});

		socket.on('reconnect', function (client) {
			filtersRequest($state.current, $state.params);
		});

		$rootScope.$on('$stateChangeSuccess', (event, toState, toParams, fromState, fromParams) => {
			if (toState.name == 'search.album')
				that.viewState = 'album';
			else
			if (toState.name == 'search.photos')
				that.viewState = 'photos';

			if (toState.name.contains('search.') && !router.isPopupState(toState.name) && (!fromState || !fromState.name || !router.isPopupState(fromState.name)))
			{
				if (isAuthenticationStateKnown)
					filtersRequest(toState, toParams);
			}
		});
	};
});