angular.module('NgApplication').service('user', function (socket, alerts, $rootScope) {
	var that = this;

	var isInitialized = false;

	this.fullName = '';
	this.isAuthenticated = () => that.fullName !== '';
	this.signIn = (isMobile) => {
	};
	this.logout = () => socket.emit('logout');

	this.initialize = () => {
		if (isInitialized)
			return;
		isInitialized = true;

		socket.on('auth-error', (result) => {
			if (result.isError)
				$rootScope.$broadcast('auth-error');
		});

		socket.on('hey', (data) => {
			console.log('-> hey');
			$rootScope.$broadcast('hey');
		});

		socket.on('hello', (data) => {
			console.log('-> hello', data);

			that.fullName = data.fullName;
			$rootScope.$broadcast('user.update');
			$rootScope.$broadcast('hello');

			alerts.add(`Welcome ${that.fullName}!`, 'success', 3000);
		});

		socket.on('bye', (data) => {
			console.log('-> bye', data);
			alerts.add(`Bye ${that.fullName}!`, 'success', 3000);

			that.fullName = '';
			$rootScope.$broadcast('user.update');
			$rootScope.$broadcast('bye');
		});
	};
});