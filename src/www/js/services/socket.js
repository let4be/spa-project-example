angular.module('NgApplication').service('socket', function ($rootScope, $exceptionHandler, socketConnection) {
	this.emit = (name, data, cb) => {
		return socketConnection.emit(name, data, (answer) => {
			if (cb) {
				try {
					cb(answer);
				} catch (error) {
					$exceptionHandler(error, 'socket.io emit callback unhandled error');
				} finally {
					if (!$rootScope.$$phase)
						$rootScope.$apply();
				}
			}
		});
	};

	this.on = (name, cb) => {
		return socketConnection.on(name, (data) => {
			if (cb) {
				try {
					cb(data);
				} catch (error) {
					$exceptionHandler(error, 'socket.io on unhandled error');
				} finally {
					if (!$rootScope.$$phase)
						$rootScope.$apply();
				}
			}
		});
	};
});