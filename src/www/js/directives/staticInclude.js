angular.module('NgApplication').directive('staticInclude', function($http, $templateCache, $compile) {
	return {
		scope: false,
		link: function (scope, element, attrs, ctrl) {
			var templatePath = attrs.staticInclude;
			$http.get(templatePath, { cache: $templateCache }).success(function(response) {
				var contents = element.html(response).contents();
				$compile(contents)(scope);
			});
		}
	};
});