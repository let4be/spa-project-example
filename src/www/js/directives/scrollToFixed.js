angular.module('NgApplication').directive('scrollToFixed', function($timeout) {
	return {
		restrict: 'A',
		scope: {
			scrollToFixed: '='
		},
		link: function (scope, element, attr) {
			var e = $(element);
			var $window = $(window);
			var originalPosition = null;
			var originalTop = null;
			var originalLeft = null;
			var originalWidth = null;
			var $pseudo = null;
			var trueLeft = e.offset().left;

			var fix = (scrollToFixed) => {
				if (scrollToFixed && scrollToFixed.isFixed()) {
					console.log('apply fixed position', scrollToFixed.top(), originalWidth);

					if ($pseudo === null) {
						$pseudo = $('<div></div>');
						$pseudo.css('width', e.width()).css('height', e.height());
						e.after($pseudo);
					}

					if (originalPosition === null) {
						originalPosition = e.css('position');
						originalLeft = e.css('left');
						originalTop = e.css('top');
						originalWidth = e.width();
					}

					if (e.css('position') != 'fixed')
						e.addClass('fixed');

					e.css('position', 'fixed');

					if ('top' in scrollToFixed)
						e.css('top', $.isFunction(scrollToFixed.top) ? scrollToFixed.top() : scrollToFixed.top);
					if ('left' in scrollToFixed)
						e.css('left', $.isFunction(scrollToFixed.left) ? scrollToFixed.left() : scrollToFixed.left);

					if ('width' in scrollToFixed)
						e.css('width', $window.width() * parseInt(scrollToFixed.width) / 100 - ('left' in scrollToFixed ? e.css('left') : trueLeft));
					else
						e.css('width', originalWidth);

					return true;
				}

				return false;
			};

			var unfix = (scrollToFixed) => {
				if ($pseudo) {
					$pseudo.remove();
					$pseudo = null;
				}

				if (originalPosition !== null)
				{
					e.css('position', originalPosition);
					e.css('left', originalLeft);
					e.css('top', originalTop);

					if (scrollToFixed && scrollToFixed.originalWidth)
						e.css('width', scrollToFixed.originalWidth);
					else
						e.css('width', 'auto');
					e.removeClass('fixed');

					$timeout(() => {
						originalWidth = e.width();
					}, 0);
				}
			};

			var refresh = (scrollToFixed) => {
				if (fix(scrollToFixed))
					return;

				unfix(scrollToFixed);
			};

			scope.$watch('scrollToFixed', refresh, true);

			var w = $window.width();
			var h = $window.height();
			var isResizing = false;

			setInterval(() => {
				if (w != $window.width() || h != $window.height())
				{
					isResizing = true;
					w = $window.width();
					h = $window.height();

					unfix(scope.scrollToFixed);

					$timeout(() => {
						fix(scope.scrollToFixed);
						isResizing = false;
					}, 1);
				}
			}, 100);

			$window.scroll(() => {
				if (!isResizing)
					refresh(scope.scrollToFixed);
			});
		}
	};
});