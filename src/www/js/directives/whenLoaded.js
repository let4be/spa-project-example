angular.module('NgApplication').directive('whenLoaded', function() {
	return {
		restrict: 'A',
		link: function (scope, element, attr) {
			element.bind('load', function () {
				scope.$apply(attr.whenLoaded);
			});
		}
	};
});