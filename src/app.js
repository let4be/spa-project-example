require('traceur/bin/traceur-runtime');
require('es6-shim');

var fs 		= require('fs'),
	path	= require('path');

var
	childProcess	= require('child_process'),
	config			= require('./modules/config');

if (process.argv.length >= 3)
	config.APP_TYPE = process.argv[2];

var apps = config.APP_TYPE.split(',');
if (apps.length > 1)
{
	for(var app of apps) {
		var env = process.env;
		env.APP_TYPE = app;

		var options = {
			cwd: __dirname,
			env: env,
			silent: false
		};

		childProcess.fork('./app', [], options);
	}

	return;
}

console.log(__dirname + '/apps');
var existingApps = fs.readdirSync(__dirname + '/apps').map(p => path.basename(p, path.extname(p)));
if (existingApps.indexOf(config.APP_TYPE) < 0)
{
	console.error(`Unsupported APP_TYPE ${config.APP_TYPE}`);
	process.exit(2);
}

console.log(`Executing application '${config.APP_TYPE}'...`);

var application = require(`./apps/${config.APP_TYPE}.js`);
application(config);

console.log(`Application '${config.APP_TYPE}' has been started`);