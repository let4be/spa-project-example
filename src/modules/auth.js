var
	chan				= require('chan'),
	co					= require('co'),
	url					= require('url'),
	request				= require('request'),
	passport			= require('passport'),
	//TwitterStrategy	= require('passport-twitter'),
	FacebookStrategy	= require('passport-facebook'),
	GooglePlusStrategy 	= require('passport-google-oauth').OAuth2Strategy,
	InstagramStrategy 	= require('passport-instagram').Strategy,
	config				= require('./config'),
	database			= require('./database'),
	db 					= database.connect(),
	WebSocketClient		= require('../modules/webSocketClient');

var auth = function(accessToken, refreshToken, profile, done)
{
	co(function* (){
		var ch = chan();

		try {
			var prefix = profile.provider.substr(0, 1);
			var id = (prefix ? prefix : 'u') + '-' + profile.id;

			db.User.findOne({_id: id}, ch);
			var user = yield ch;
			if (!user)
				user = new db.User({
					_id: id
				});
			user.fullName = profile.displayName ? profile.displayName : profile.username;

			if (profile._json.gender)
				user.gender = profile._json.gender;

			if (profile._json.locale)
				user.locale = profile._json.locale;

			if (profile._json.timezone)
				user.timezone = profile._json.timezone;

			user.save(ch);
			yield ch;

			return done(null, user);
		} catch (err) {
			console.error('auth error', error.stack);
			return done(err);
		}
	})();
};

passport.serializeUser(function(user, done) {
	console.log('serializeUser', user._id);
	done(null, user._id);
});

passport.deserializeUser(function(id, done) {
	console.log('deserializeUser', id);
	db.User.findById(id, (err, user) => {
		done(err, user);
	});
});

passport.use(new FacebookStrategy({
		clientID:  config.FACEBOOK_APP_ID,
		clientSecret:  config.FACEBOOK_APP_SECRET,
		callbackURL:  config.FACEBOOK_CALLBACK,
		enableProof: false
	},
	function(token, tokenSecret, profile, done) {
		console.log(`facebook callback token: ${token}, tokenSecret: ${tokenSecret}, profile: `, profile);

		auth(token, tokenSecret, profile, done);
	}
));

passport.use(new GooglePlusStrategy({
		clientID: config.GOOGLE_CLIENT_ID,
		clientSecret: config.GOOGLE_CLIENT_SECRET,
		callbackURL: config.GOOGLE_CALLBACK
	},
	function(token, tokenSecret, profile, done) {
		console.log(`google callback token: ${token}, tokenSecret: ${tokenSecret}, profile: `, profile);

		auth(token, tokenSecret, profile, done);
	}
));

passport.use(new InstagramStrategy({
		clientID: config.INSTAGRAM_CLIENT_ID,
		clientSecret: config.INSTAGRAM_CLIENT_SECRET,
		callbackURL: config.INSTAGRAM_CALLBACK
	},
	function(token, tokenSecret, profile, done) {
		console.log(`instagram callback token: ${token}, tokenSecret: ${tokenSecret}, profile: `, profile);

		auth(token, tokenSecret, profile, done);
	}
));

/*passport.use(new TwitterStrategy({
		consumerKey: config.TWITTER_CONSUMER_KEY,
		consumerSecret: config.TWITTER_CONSUMER_SECRET,
		callbackURL: config.TWITTER_CALLBACK
	},
	function(token, tokenSecret, profile, done) {
		console.log(`twitter callback token: ${token}, tokenSecret: ${tokenSecret}, profile: `, profile);

		auth(token, tokenSecret, profile, done);
	}
));*/

module.exports = function(app, redis)
{
	app.use(passport.initialize());
	app.use(passport.session());

	/*app.get('/auth/twitter', function(req, res, next){
		req.session.isPopupAuth = req.query.popup;
		return passport.authenticate('twitter')(req, res, next);
	});

	app.get('/auth/twitter/callback',
		passport.authenticate('twitter', { successRedirect: '/auth/close', failureRedirect: '/auth/close' }));*/

	app.get('/auth/facebook', function(req, res, next) {
		req.session.isPopupAuth = req.query.popup;
		return passport.authenticate('facebook')(req, res, next);
	});

	app.get('/auth/facebook/callback',
		passport.authenticate('facebook', { successRedirect: '/auth/close', failureRedirect: '/auth/close' }));

	app.get('/auth/google-plus', function(req, res, next) {
		req.session.isPopupAuth = req.query.popup;
		return passport.authenticate('google', {scope: 'https://www.googleapis.com/auth/userinfo.profile'})(req, res, next);
	});

	app.get('/auth/google-plus/callback',
		passport.authenticate('google', { successRedirect: '/auth/close', failureRedirect: '/auth/close' }));

	app.get('/auth/instagram', function(req, res, next) {
		req.session.isPopupAuth = req.query.popup;
		return passport.authenticate('instagram')(req, res, next);
	});

	app.get('/auth/instagram/callback',
		passport.authenticate('instagram', { successRedirect: '/auth/close', failureRedirect: '/auth/close' }));


	var notifyClientAboutAuth = (req, isError) => {
		var redisPrivateChannel = WebSocketClient.getPrivateRoomNameBySidCookie(req.cookies['connect.sid']);
		redis.publish(redisPrivateChannel, isError ? 'auth-error' : 'auth-ok');
	};

	app.get('/auth/close', function(req, res, next) {
		console.log('!!!!! req.session', req.session);

		if (req.session.passport && req.session.passport.user)
		{
			console.log('req.session.passport.user', req.cookies['connect.sid']);

			var time = 30 * 24 * 60 * 60 * 1000;
			req.session.cookie.expires = new Date(Date.now() + time);
			req.session.cookie.maxAge = time;
			req.session.bump = Date.now();
			req.session.save(function(err){
				if (err) {
					console.error('cannot save the updated session', err);
				}

				notifyClientAboutAuth(req, !!err);

				if (req.session.isPopupAuth)
					res.send('<html><head><script>window.close();</script></head><body>Done!</body></html>');
				else
					res.redirect('/');
			});

			return;
		}
		console.error('no req.session.passport.user found');

		if (req.session.isPopupAuth) {
			notifyClientAboutAuth(req, true);
			res.send('<html><head><script>window.close();</script></head><body>Something went wrong :(</body></html>');
		}
		else
			res.send('<html><head><script>setTimeout(function(){window.close();}, 3000);</script></head><body>Something went wrong :(</body></html>');
	});
};