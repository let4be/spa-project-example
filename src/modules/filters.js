var chan			= require('chan'),
	co				= require('co'),
	moment			= require('moment'),
	database		= require('./database'),
	mongoose		= require('mongoose'),
	db				= database.connect(),
	RedisClient		= require('./redis'),
	redis			= RedisClient(),
	config			= require('./config');

var Filters = function(filters) {
	var that = this;

	this.data = filters;
	this.getFiltersCasely = (filtersName, everything) => {
		var f = filters[filtersName].filter(x => x.isSelected).map(x => x.id);
		return everything && f.findIndex(x => x.toLowerCase() == everything.toLowerCase()) > -1 ? false : f.map(x => x.toLowerCase());
	};
	this.getFilters = (filtersName, everything) => {
		var f = filters[filtersName].filter(x => x.isSelected).map(x => x.id);
		return everything && f.findIndex(x => x.toLowerCase() == everything.toLowerCase()) > -1 ? false : f;
	};
	this.getBoolFilter = (filtersName, value) => {
		var f = that.getFiltersCasely(filtersName, '-');
		return f.length > 0 && f[0] == value.toLowerCase();
	};

	this.isAlbums = this.getBoolFilter('kind', 'albums');
	this.isRecent = this.getBoolFilter('time', 'recent');

	this.schoolFilters = this.getFilters('schools', 'national');
	this.sportTypeFilters = this.getFiltersCasely('sports', 'all');
	this.sportLevelFilters = this.getFiltersCasely('sportLevel');
	this.genderFilters = this.getFiltersCasely('gender');

	this.for = filters.for;
	this.offset = 0;
	this.limit = 40;

	var buildQuery = function* (personalInappropriateEventFiles) {
		var pipeline = [];

		var match = {
			albumStatus: 'live'
		};
		var matchAnd = [];

		if (filters.eventId) {
			match._id = mongoose.Types.ObjectId(filters.eventId);
		}

		if (that.schoolFilters !== false && that.schoolFilters.length > 0)
		{
			var schoolIds = that.schoolFilters.map(x => mongoose.Types.ObjectId(x));
			matchAnd.push({
				$or: [
					{home: {$in: schoolIds}},
					{visitor: {$in: schoolIds}}
				]
			});
		}

		if (that.sportTypeFilters !== false && that.sportTypeFilters.length > 0)
			match.sportType = {$in: that.sportTypeFilters};

		if (that.sportLevelFilters !== false && that.sportLevelFilters.length > 0) {
			var otherIdx = that.sportLevelFilters.findIndex(x => x == 'other');
			if (otherIdx > -1) {
				if (that.sportLevelFilters.indexOf('varsity') < 0 || that.sportLevelFilters.indexOf('jv') < 0)
					matchAnd.push({
						$or: [
							{sportLevel : {$in: that.sportLevelFilters}},
							{sportLevel : {$nin: ['varsity', 'jv']}}
						]
					});
			} else
				match.sportLevel = {$in: that.sportLevelFilters};
		}

		if (that.genderFilters !== false && that.genderFilters.length > 0)
			match.gender = {$in: that.genderFilters};

		if (matchAnd.length > 0)
			match.$and = matchAnd;

		if (Object.keys(match).length > 0) {
			pipeline.push({
				$match: match
			});
			match = {};
		}

		pipeline.push({
			$unwind: '$s3list'
		});

		pipeline.push({$match: {
			's3list.isProcessing': false,
			$or: [
				{'s3list.isInappropriateMark': {$exists: false}},
				{'s3list.isInappropriateMark': false}
			]
		}});

		if (personalInappropriateEventFiles && personalInappropriateEventFiles.length > 0)
			pipeline.push({$match: {
				's3list._id': {
					$nin: personalInappropriateEventFiles
				}
			}});

		if (Object.keys(match)) {
			pipeline.push({
				$match: match
			});
		}

		pipeline.push({
			$sort: that.isRecent ? {
				'date': -1,
				's3list.nameIndex1': 1,
				's3list.nameIndex2': 1,
				's3list.nameIndex3': 1,
				's3list.nameIndex4': 1,
				's3list.nameIndex5': 1
			}
			: {
				'date': -1,
				's3list.likesCounter': -1,
				's3list.nameIndex1': 1,
				's3list.nameIndex2': 1,
				's3list.nameIndex3': 1,
				's3list.nameIndex4': 1,
				's3list.nameIndex5': 1
			}
		});


		if (that.isAlbums) {
			pipeline.push({
				$group: {
					_id: '$_id',
					eventType: { $first: '$eventType' },
					eventCustomTitle: { $first: '$eventCustomTitle' },
					sportType: { $first: '$sportType' },
					sportLevel: { $first: '$sportLevel' },
					gender: { $first: '$gender' },
					home: { $first: '$home' },
					visitor: { $first: '$visitor' },
					date: { $first: '$date' },
					s3list: { $first: '$s3list' }
				}
			});

			pipeline.push({
				$sort: {
					'date': -1
				}
			});
		}

		pipeline.push({
			$project: {
				_id: false,
				eventId: '$_id',
				eventType: 1,
				eventCustomTitle: 1,
				sportType: 1,
				sportLevel: 1,
				gender: 1,
				home: 1,
				visitor: 1,
				date: 1,
				name: '$s3list.name',
				isInappropriateMark: '$s3list.isInappropriateMark',
				likesCounter: '$s3list.likesCounter'
			}
		});

		if (!filters.isFull) {
			if (that.offset)
				pipeline.push({
					$skip: that.offset
				});

			if (that.limit)
				pipeline.push({
					$limit: that.limit
				});
		}

		return pipeline;
	};

	this.queryMongo = function* (personalInappropriateEventFiles) {
		var ch = chan();
		var ds = Date.now();
		var events = null;
		var timeTaken = 0;

		var redisKey = '/web/images/json/' + JSON.stringify(filters);
		if (!filters.isFull)
			redisKey += '/offset/' + this.offset + '/limit/' + this.limit;

		redis.get(redisKey, ch);
		var redisValue = yield ch;
		if (redisValue) {
			events = JSON.parse(redisValue);
			timeTaken = Date.now() - ds;

			console.log(`images aggregation exctracted from redis for ${timeTaken} ms., found ${events.length} ent.`);

			return JSON.parse(redisValue);
		}

		var pipeline = yield buildQuery(personalInappropriateEventFiles);

		//console.log('aggregate mongo events with pipeline', JSON.stringify(pipeline, null, 2));
		db.Event.aggregate(pipeline, ch);
		events = yield ch;

		timeTaken = Date.now() - ds;

		if (timeTaken > 500)
			console.warn(`images aggregation took ${timeTaken} ms., found ${events.length} ent.`);
		else
			console.log(`images aggregation took ${timeTaken} ms., found ${events.length} ent.`);

		var ids = [];
		(function(){
			for(var event of events)
			{
				if (mongoose.Types.ObjectId.isValid(event.home.toString()) && !ids[event.home])
					ids[event.home] = true;

				if (mongoose.Types.ObjectId.isValid(event.visitor.toString()) && !ids[event.visitor])
					ids[event.visitor] = true;
			}
			ids = Object.keys(ids);
		})();

		console.log(`images aggregation, need to pull ${ids.length} schools...`);
		db.School.find({_id: {$in: ids}}, {_id: true, name: true, longName: true}, ch);
		var schoolsArray = yield ch;

		var schoolNames = schoolsArray && schoolsArray.length > 0 ? schoolsArray.reduce((a, c) => {
			if (c)
				a[c._id] = (c.name ? c.name : c.longName).trim();
			return a;
		}, {}) : {};

		(function () {
			for (var event of events) {
				event.home = event.home && schoolNames[event.home] ? schoolNames[event.home] : 'unknown';
				event.visitor = event.visitor && schoolNames[event.visitor] ? schoolNames[event.visitor] : 'unknown';
				event.date = moment(event.date).format('MMM DD, YYYY');
			}
		})();

		timeTaken = Date.now() - ds;
		if (timeTaken > 500)
			console.warn(`images aggregation with schools lookup took ${timeTaken} ms.`);
		else
			console.log(`images aggregation with schools lookup took ${timeTaken} ms.`);

		co(function *(){
			redis.set(redisKey, JSON.stringify(events), 'NX', 'EX', config.REDIS_CACHE_TIME_FOR_IMAGES, ch);
			yield ch;
		})();

		return events;
	};
};

var Filter = function (opt) {
	this.name = opt.name ? opt.name : opt.longName;
	this.isSelected = opt.isSelected;

	this.id = opt.id ? opt.id : this.name;
};

var kindAlbums = new Filter({name: 'ALBUMS', isSelected: true});
var kindPhotos = new Filter({name: 'PHOTOS', isSelected: false});

Filters.default = {
	schools: [new Filter({
		id: 0, name: "NATIONAL", isSelected: true
	})],
	sports: [
		new Filter({name: 'ALL', isSelected: true}),
		new Filter({name: 'FOOTBALL', isSelected: false}),
		new Filter({name: 'BASKETBALL', isSelected: false}),
		new Filter({name: 'BASEBALL', isSelected: false}),
		new Filter({name: 'SOCCER', isSelected: false}),
		new Filter({name: 'TENNIS', isSelected: false}),
		new Filter({name: 'LACROSSE', isSelected: false}),
		new Filter({name: 'VOLLEYBALL', isSelected: false}),
		new Filter({name: 'SWIMMING', isSelected: false}),
		new Filter({name: 'DIVING', isSelected: false}),
		new Filter({name: 'GYMNASTICS', isSelected: false})
	],
	time: [
		new Filter({name: 'RECENT', isSelected: true}),
		new Filter({name: 'POPULAR', isSelected: false})
	],
	kind: [
		kindAlbums,
		kindPhotos
	],
	gender: [
		new Filter({name: 'BOYS', isSelected: true}),
		new Filter({name: 'GIRLS', isSelected: true})
	],
	sportLevel: [
		new Filter({name: 'VARSITY', isSelected: true}),
		new Filter({name: 'JV', isSelected: true}),
		new Filter({name: 'OTHER', isSelected: false})
	],
	eventId: '',
	eventIds: [],
	isFull: false
};

module.exports = Filters;