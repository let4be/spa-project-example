var url		= require("url"),
	redis	= require("redis");

if (!process.env.REDISCLOUD_URL)
{
	console.error('REDISCLOUD_URL environment variable required!');
	process.exit(2);
}
var REDIS_URL = url.parse(process.env.REDISCLOUD_URL.trim());
var host = REDIS_URL.host.split(':');
var auth = REDIS_URL.auth ? REDIS_URL.auth.split(':') : null;

console.log('host', host);
console.log('auth', auth);

module.exports = (options = {}) => {
	if (auth)
		options.pass = auth[1];

	var client = redis.createClient(REDIS_URL.port, host[0], options);

	client.on('error', function (err) {
		console.error('redis error ', err);
	});

	if (options.pass)
		client.auth(options.pass);


	return client;
};