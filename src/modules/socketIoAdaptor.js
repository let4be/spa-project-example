var chan = require('chan');

var SocketIoAdaptorToWebSocket = function (ws) {
	var self = this;

	this.request = {
		connection: {
			remoteAddress: ws.upgradeReq.connection.remoteAddress
		},
		headers: ws.upgradeReq.headers
	};

	this.handshake = {
		cookies: null,
		session: {},
		headers: ws.upgradeReq.headers
	};
	var onHandlers = {};

	var answer = (type, uid, data) => {
		data._uid = uid;
		self.emit(`${type}-answer`, data);
	};

	var currentUid = 0;

	var handleMessage = (type, uid, message) =>
	{
		if (type in onHandlers) {
			console.log('WSS handle message', type, uid, message);
			onHandlers[type](message, (answerMessage) => {
				answer(type, uid, answerMessage);
			});
		} else {
			if (isReady) {
				answer(type, uid, 'Unknown message!');
				console.log('WSS skip message', type, uid);
			} else
				queue.push({
					type: type,
					uid: uid,
					message: message
				});
		}
	};

	var isReady = false;
	var queue = [];
	this.ready = () => {
		isReady = true;

		for(var e in queue)
		{
			e = queue[e];
			handleMessage(e.type, e.uid, e.message);
		}

		queue = [];
	};

	ws.on('message', (incomingMessage) => {
		var m = null;
		var type = null;
		var uid = null;

		try {
			m = JSON.parse(incomingMessage);
			if ('_type' in m) {
				type = m._type;
				delete m._type;
			}
			if ('_uid' in m) {
				uid = m._uid;
				delete m._uid;
			}
		} catch (error) {
			answer('error', null, 'Invalid message format!');
			return;
		}

		if (type === null) {
			answer('error', null, `Message type not found!`);
			return;
		}

		if (uid === null) {
			answer('error', null, `Sequence number not found!`);
			return;
		}

		if (uid != currentUid) {
			answer('error', null, `Incorrect sequence number - ${uid} found but ${currentUid} expected`);
			return;
		}
		currentUid++;

		handleMessage(type, uid, m);
	});

	this.emit = (type, data) => {
		data._type = type;
		ws.send(JSON.stringify(data), (error) => {
			if (error)
				console.warn('error emitting socket.io message', error.stack);
		});
	};

	this.on = (event, callback) => {
		//console.log('add event handler for ', event);
		onHandlers[event] = callback;
	};

	this.join = () => {

	};
};


module.exports = SocketIoAdaptorToWebSocket;