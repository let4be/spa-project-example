var
	url		= require('url'),
	error	= require('./error');

var HOSTNAME = require('os').hostname();
var IS_DEV = HOSTNAME == 'dev';

if (!process.env.AWS_REGION)
{
	console.error('AWS_REGION environment variable required!');
	process.exit(1);
}

if (!process.env.REDIS_CACHE_TIME_FOR_IMAGES)
{
	console.error('REDIS_CACHE_TIME_FOR_IMAGES environment variable required!');
	process.exit(1);
}

if (!process.env.AWS_ACCESS_KEY_ID)
{
	console.error('AWS_ACCESS_KEY_ID environment variable required!');
	process.exit(1);
}

if (!process.env.AWS_SECRET_KEY)
{
	console.error('AWS_SECRET_KEY environment variable required!');
	process.exit(1);
}

if (!process.env.TWITTER_CONSUMER_KEY)
{
	console.error('TWITTER_CONSUMER_KEY environment variable required!');
	process.exit(1);
}

if (!process.env.TWITTER_CONSUMER_SECRET)
{
	console.error('TWITTER_CONSUMER_SECRET environment variable required!');
	process.exit(1);
}

if (!process.env.TWITTER_CALLBACK)
{
	console.error('TWITTER_CALLBACK environment variable required!');
	process.exit(1);
}

if (!process.env.FACEBOOK_APP_ID)
{
	console.error('TWITTER_CONSUMER_KEY environment variable required!');
	process.exit(1);
}

if (!process.env.FACEBOOK_APP_SECRET)
{
	console.error('TWITTER_CONSUMER_SECRET environment variable required!');
	process.exit(1);
}

if (!process.env.FACEBOOK_CALLBACK)
{
	console.error('TWITTER_CALLBACK environment variable required!');
	process.exit(1);
}

if (!process.env.GOOGLE_CLIENT_ID)
{
	console.error('GOOGLE_ID environment variable required!');
	process.exit(1);
}

if (!process.env.GOOGLE_CLIENT_SECRET)
{
	console.error('GOOGLE_SECRET environment variable required!');
	process.exit(1);
}

if (!process.env.GOOGLE_CALLBACK)
{
	console.error('GOOGLE_CALLBACK environment variable required!');
	process.exit(1);
}

if (!process.env.INSTAGRAM_CLIENT_ID)
{
	console.error('INSTAGRAM_ID environment variable required!');
	process.exit(1);
}

if (!process.env.INSTAGRAM_CLIENT_SECRET)
{
	console.error('INSTAGRAM_SECRET environment variable required!');
	process.exit(1);
}

if (!process.env.INSTAGRAM_CALLBACK)
{
	console.error('INSTAGRAM_CALLBACK environment variable required!');
	process.exit(1);
}

if (!process.env.MAILGUN_API_KEY)
{
	console.error('MAILGUN_API_KEY environment variable required!');
	process.exit(1);
}

if (!process.env.MAILGUN_DOMAIN)
{
	console.error('MAILGUN_DOMAIN environment variable required!');
	process.exit(1);
}

if (!process.env.CONTACT_EMAIL_FROM)
{
	console.error('CONTACT_EMAIL_FROM environment variable required!');
	process.exit(1);
}

if (!process.env.CONTACT_EMAIL_TO)
{
	console.error('CONTACT_EMAIL_TO environment variable required!');
	process.exit(1);
}

if (!process.env.RECAPTCHA_PRIVATE_KEY)
{
	console.error('RECAPTCHA_PRIVATE_KEY environment variable required!');
	process.exit(1);
}

if (!process.env.RECAPTCHA_PUBLIC_KEY)
{
	console.error('RECAPTCHA_PUBLIC_KEY environment variable required!');
	process.exit(1);
}

if (!process.env.APP_LOC)
{
	console.error('APP_LOC environment variable required!');
	process.exit(1);
}

if (!process.env.APP_PUBLIC_ADDRESS)
{
	console.error('APP_PUBLIC_ADDRESS environment variable required!');
	process.exit(1);
}


console.log(`Running ${process.version} node.js version at ${HOSTNAME}`);
console.log(`Is development: ${!!IS_DEV}`);

class Config {
	constructor(){
		this.IS_DEV = IS_DEV;
		this.WWW_ROOT = __dirname + '/../www';
		this.WEB_PORT = Number(process.env.PORT || 5000);

		this.APP_TYPE = process.env.APP_TYPE ? process.env.APP_TYPE.trim() : '';
		this.AWS_REGION = process.env.AWS_REGION.trim();
		this.AWS_ACCESS_KEY_ID = process.env.AWS_ACCESS_KEY_ID.trim();
		this.AWS_SECRET_KEY = process.env.AWS_SECRET_KEY.trim();
		this.TWITTER_CONSUMER_KEY = process.env.TWITTER_CONSUMER_KEY.trim();
		this.TWITTER_CONSUMER_SECRET = process.env.TWITTER_CONSUMER_SECRET.trim();
		this.TWITTER_CALLBACK = process.env.TWITTER_CALLBACK.trim();
		this.GOOGLE_CLIENT_ID = process.env.GOOGLE_CLIENT_ID.trim();
		this.GOOGLE_CLIENT_SECRET = process.env.GOOGLE_CLIENT_SECRET.trim();
		this.GOOGLE_CALLBACK = process.env.GOOGLE_CALLBACK.trim();
		this.INSTAGRAM_CLIENT_ID = process.env.INSTAGRAM_CLIENT_ID.trim();
		this.INSTAGRAM_CLIENT_SECRET = process.env.INSTAGRAM_CLIENT_SECRET.trim();
		this.INSTAGRAM_CALLBACK = process.env.INSTAGRAM_CALLBACK.trim();
		this.FACEBOOK_APP_ID = process.env.FACEBOOK_APP_ID.trim();
		this.FACEBOOK_APP_SECRET = process.env.FACEBOOK_APP_SECRET.trim();
		this.FACEBOOK_CALLBACK = process.env.FACEBOOK_CALLBACK.trim();
		this.MAILGUN_API_KEY=process.env.MAILGUN_API_KEY.trim();
		this.MAILGUN_DOMAIN=process.env.MAILGUN_DOMAIN.trim();
		this.CONTACT_EMAIL_FROM=process.env.CONTACT_EMAIL_FROM.trim();
		this.CONTACT_EMAIL_TO=process.env.CONTACT_EMAIL_TO.trim();
		this.RECAPTCHA_PRIVATE_KEY = process.env.RECAPTCHA_PRIVATE_KEY.trim();
		this.RECAPTCHA_PUBLIC_KEY = process.env.RECAPTCHA_PUBLIC_KEY.trim();
		this.APP_LOC = process.env.APP_LOC.trim();
		this.APP_PUBLIC_ADDRESS = process.env.APP_PUBLIC_ADDRESS.trim();
		this.DYNO = process.env.DYNO ? process.env.DYNO.trim() : 'default';
		this.REDIS_CACHE_TIME_FOR_IMAGES = process.env.REDIS_CACHE_TIME_FOR_IMAGES.trim();

		Config.Instance = this;
	}
}

module.exports = new Config();