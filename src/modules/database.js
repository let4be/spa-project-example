var
	crypto		= require('crypto'),
	mongoose	= require('mongoose'),
	Schema 		= mongoose.Schema,
	ObjectId	= Schema.ObjectId;

if (!process.env.MONGOLAB_URI) {
	console.error('MONGOLAB_URI environment variable required!');
	process.exit(1);
}

function DataContext(db) {
	var that = this;

	var sportsEnum = ['football', 'basketball', 'baseball', 'soccer', 'tennis', 'lacrosse', 'volleyball', 'swimming',
		'diving', 'gymnastics' ];

	var sportLevelEnum = ['varsity', 'jv', 'freshmanA', 'freshmanB', 'sophomore'];
	var genderEnum = ['boys', 'girls'];
	var statesEnum = ['AL','AK','AZ','AR',
		'CA','CO','CT',
		'DE',
		'FL',
		'GA',
		'HI',
		'ID','IL','IN','IA',
		'KS','KY',
		'LA',
		'ME','MD','MA','MI','MN','MS','MO','MT',
		'NE','NV','NH','NJ','NM','NY','NC','ND',
		'OH','OK','OR',
		'PA',
		'RI',
		'SC','SD',
		'TN','TX',
		'UT',
		'VT',
		'VA',
		'WA','WV','WI','WY'];

	this.UserSchema = new Schema({
		_id: {type: String},
		settings: {},
		email: {type: String},
		passwordHash: {type: String, set: (function (v) {
			return v ? crypto.createHash('sha256').update(v).digest('hex') : '';
		})},
		fullName: {type: String},
		gender: {Type: String},
		locale: {Type: String},
		timezone: {type: Number},
		favoriteSchools: [{type: ObjectId, ref: 'School'}],
		filters: {type: String},
		inappropriateEventFiles: [{type: ObjectId}]
	});

	this.SchoolSchema = new Schema({
		_id: {type: ObjectId},
		name: {type: String, default: ''},
		longName: {type: String, required: true},
		mascot:
		{
			name: {type: String},
			url: {type: String}
		},
		address1: {type: String},
		address2: {type: String},
		city: {type: String, required: true},
		state: {type: String, required: true, enum: statesEnum},
		zip: {type: String},
		sports: [{type: String, enum: sportsEnum}],
		colors:
		{
			primary: {type: String, match: '^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$'},
			secondary: {type: String, match: '^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$'},
			tertiary: {type: String, match: '^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$'}
		},
		events: [{type: ObjectId, ref: 'Event'}],
		metrics:
		{
			totals: {
				events: {type: Number},
				photographs: {type: Number},
				uniqueVisitors: {type: Number},
				followers: {type: Number}
			}
		},
		isActive: {type: Boolean, default: false, index: true}
	});
	this.SchoolSchema.index({isActive: 1});
	this.SchoolSchema.set('autoIndex', true);

	this.EventFileSchema = new Schema({
		name: {type: String, required: true},
		nameIndex1: {type: Number, default: 0},
		nameIndex2: {type: Number, default: 0},
		nameIndex3: {type: Number, default: 0},
		nameIndex4: {type: Number, default: 0},
		nameIndex5: {type: Number, default: 0},
		s3ConfigHash: {type: String, default: ''},
		isProcessing: {type: Boolean, default: false},
		isInappropriateMark: {type: Boolean, default: false},
		viewsCounter: {type: Number, default: 0},
		likesCounter: {type: Number, default: 0}
	});
	this.EventFileSchema.index({name: 1, nameIndex1: 1, nameIndex2: 1, nameIndex3: 1, nameIndex4: 1, nameIndex5: 1, likesCounter: 1});
	this.EventFileSchema.set('autoIndex', true);

	this.EventFileCommentSchema = new Schema({
		text: {type: String, required: true},
		date: {type: Date, default: Date.now},
		user: {type: String, ref: 'User'}
	});

	this.ImageStatSchema = new Schema({
		eventId: {type: ObjectId, required: true},
		name: {type: String, required: true},

		errorStack: {type: String, default: null},
		errorMessage: {type: String, default: null},
		processedDate: {type: Date, default: null},
		processingTimeMs: {type: Number, default: 0},

		viewsCounter: {type: Number, default: 0},
		likesCounter: {type: Number, default: 0},
		likedBy: [
			{type: String, ref: 'User'}
		],
		comments: [that.EventFileCommentSchema]
	});
	this.ImageStatSchema.index({eventId: 1, name: 1}, {unique: true});
	this.ImageStatSchema.set('autoIndex', true);

	// s3 node synchronizes s3list with s3 bucket
	this.EventSchema = new Schema({
		_id: {type: ObjectId},
		eventType: {type: String, enum: ['AB', 'conference', 'scrimmage']},
		eventCustomTitle: {type: String, default: ''},
		sportType: {type: String, enum: sportsEnum},
		sportLevel: {type: String, enum: sportLevelEnum},
		gender: {type: String, enum: genderEnum},
		home: {type: ObjectId, ref: 'School', default: null},
		visitor: {type: ObjectId, ref: 'School', default: null},
		date: {type: Date, default: Date.now},
		highScore: {type: Number},
		lowScore: {type: Number},
		winner: {type: String, enum: ['h', 'v', ''], default: ''},
		assignedPhotographer: {type: ObjectId, ref: 'Photographer', default: null},
		metrics: {
			totals: {
				photographs: {type: Number},
				approvedPhotographs: {type: Number},
				clicks: {type: Number},
				socialShares: {
					facebook: {type: Number},
					instagram: {type: Number},
					twitter: {type: Number}
				}
			}
		},
		albumStatus: {type: String, enum: ['uploaded', 'in-review', 'approved', 'processing', 'live']},
		s3list: [that.EventFileSchema],
		s3listOptionsChecksum: {type: String, default: ''}
	});
	this.EventSchema.index({sportType: 1, sportLevel: 1, gender: 1, home: 1, visitor: 1, date: 1});
	this.EventSchema.set('autoIndex', true);

	this.PhotographSchema = new Schema({
		//TDB
	});

	this.PhotographerSchema = new Schema({
		firstName: {type: String},
		lastName: {type: String},
		isActive: {type: Boolean, default: true},
		events: [{type: ObjectId, ref: 'Event'}],
		rating: {type: Number, maximum: 5, minimum: 1, default: 3}
	});

	this.S3Config = new Schema({
		detailWidth: {type: Number, default: 1920},
		detailMaxHeight: {type: Number, default: 1080},
		mobileWidth: {type: Number, default: 1080},
		mobileMaxHeight: {type: Number, default: 607},
		thumbWidth: {type: Number, default: 272},
		thumbMaxHeight: {type: Number, default: 177},
		isAutoOrient: {type: Boolean, default: true},
		isNormalize: {type: Boolean, default: true}
	});

	this.Config = new Schema({
		isReprocessImagesOnS3ConfigChange: {type: Boolean, default: true},
		s3Concurency: {type: Number, default: 100, min: 1, max: 150}
	});

	// real world models
	this.User = db.model('User', this.UserSchema);
	this.School = db.model('School', this.SchoolSchema);
	this.Event = db.model('Event', this.EventSchema);
	this.Photographer = db.model('Photographer', this.PhotographerSchema);
	this.Photograph = db.model('Photograph', this.PhotographSchema);
	//

	// we have to mitigate additional complexity of nested collections by introducing "this"
	this.ImageStat = db.model('ImageStat', this.ImageStatSchema);
	//

	// dynamic configuration
	this.S3Config = db.model('S3Config', this.S3Config);
	this.Config = db.model('Config', this.Config);
	//

	this.ObjectId = ObjectId;
}

function Database() {
	var context = null;

	this.connect = () => {
		if (!context)
			context = new DataContext(mongoose.connect(process.env.MONGOLAB_URI.trim()));
		return context;
	};
}

module.exports = new Database();