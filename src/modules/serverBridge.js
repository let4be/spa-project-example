var ServerBridge = function (server) {
	var webUpgrade = null;
	var mobileUpgrade = null;

	var Bridge = function (type) {
		var id = 0;

		server.on('upgrade', (req, socket, upgradeHead) => {
			if (req.isProcessed)
				return;

			console.log(`ServerBridge upgrade `, req.headers.cookie, req.url);

			if (req.url.startsWith('/socket.io/')) {
				if (webUpgrade) {
					req.isProcessed = true;
					webUpgrade(req, socket, upgradeHead);
					console.log('ServerBridge upgrade: go web');
				}
			}
			else {
				if (mobileUpgrade) {
					req.isProcessed = true;
					mobileUpgrade(req, socket, upgradeHead);
					console.log('ServerBridge upgrade: go mobile');
				}
			}
		});

		this.on = (event, callback) => {
			if (event == 'upgrade') {
				console.log(`ServerBridge bind upgrade: for ${type}`);

				if (type == 'web')
					webUpgrade = callback;
				else if (type == 'mobile')
					mobileUpgrade = callback;
				else {
					console.error(`ServerBridge: unknown bridge type ${type}`);
					throw new Error('wtf?!');
				}
			}
			else
				server.on(event, callback);
		};

		this.once = (event, callback) => {
			if (event == 'upgrade') {
				console.log(`ServerBridge bind upgrade: for ${type}`);

				if (type == 'web')
					webUpgrade = callback;
				else if (type == 'mobile')
					mobileUpgrade = callback;
				else
					console.error(`ServerBridge: unknown bridge type ${type}`);
			}
			else
				server.once(event, callback);
		};
	};


	Bridge.prototype = server;

	return Bridge;
};

module.exports = ServerBridge;