class InternalError extends Error {
	constructor(){
		this.name = 'InternalError';
	}
}

class LogicError extends Error {
	constructor(){
		this.name = 'LogicError';
	}
}

global.InternalError = InternalError;
global.LogicError = LogicError;