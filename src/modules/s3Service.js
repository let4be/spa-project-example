var path	= require('path');

var S3Service = function (s3) {
	this.head = (bucketName, ch) => s3.headBucket({Bucket: bucketName}, ch);
	this.create = (bucketName, ch) => s3.createBucket({Bucket: bucketName}, ch);
	this.get = (bucketName, key, ch) => s3.getObject({Bucket: bucketName, Key: key}, ch);
	this.put = (bucketName, key, body, ch) => s3.putObject({Bucket: bucketName, Key: key, Body: body}, ch);
	this.listPhotos = (bucketName, eventId, name, ch) => s3.listObjects({Bucket: bucketName, Prefix: `${eventId}/${name}/`, Delimiter: '/'}, ch);
	this.listEvents = (bucketName, ch) => s3.listObjects({Bucket: bucketName, Delimiter: '/'}, ch);

	this.transformList = (list) => list.Contents.reduce((t, c) => {
		var key = path.basename(c.Key).toUpperCase();
		if (key.endsWith('.JPG') || key.endsWith('.JPEG'))
			t[key] = c;
		return t;
	}, {});
};

module.exports = S3Service;