var
	crypto				= require('crypto'),
	chan				= require('chan'),
	co					= require('co'),
	moment				= require('moment'),
	mongoose			= require('mongoose'),
	Mailgun				= require('mailgun').Mailgun,
	Recaptcha			= require('recaptcha').Recaptcha,
	ElasticSearchClient = require('elasticsearchclient'),

	database			= require('./database'),
	db					= database.connect(),

	Filters				= require('./filters');

var elasticSearchClient = new ElasticSearchClient({
	host: 'bofur-us-east-1.searchly.com',
	port: 80,
	secure: false,
	auth: {
		username: 'site',
		password: 'fd680dfc20f1f581e250ecbe0e68e863'
	}
});

function escapeRegExp(str) {
	return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

function VerboseError(message, errorCode) {
	this.name = 'VerboseError';
	this.message = (message || '');
	this.errorCode = errorCode;
}
VerboseError.prototype = Error.prototype;

var WebSocketClient = function (socket, handshake, redis, config, isMobile)
{
	var session = socket.handshake.session;
	var mailgun = new Mailgun(config.MAILGUN_API_KEY);

	var messagesQueue = [];

	if (!session.favoriteSchools)
		session.favoriteSchools = [];

	/* jshint ignore:start */
	var onMessage = (queueName, messageName, messageHandler, errorHandler = null, finalHandler = null) =>
	{
		var defaultErrorHandler = function(err)
		{
			console.error(`socket.io ${messageName} exception: `, err.stack);
		};

		socket.on(messageName, (data, cb) => {
			console.log(`-> ${messageName}`, data && data.length > 1024 ? data.substr(0, 1024) : data);

			var cor = function* (){
				var ch = chan();

				var callback = (data) => {
					if (cb) {
						cb(data);
						cb = null;
					}
				};

				try {
					yield messageHandler(data, ch, callback);

					callback({});
				} catch (err) {
					defaultErrorHandler(err);

					if (err.name == 'VerboseError')
						return callback({error: err.message, errorCode: err.errorCode});

					if (errorHandler)
						yield errorHandler(err, ch, callback);

					callback({error: 'something went wrong, try again later!'});
				} finally {
					if (finalHandler)
						yield finalHandler(ch, callback);

					messagesQueue[queueName] = messagesQueue[queueName].slice(1);
					if (messagesQueue[queueName].length > 0)
						co(messagesQueue[queueName][0])();
				}
			};

			var queued = false;
			if (!messagesQueue[queueName])
				messagesQueue[queueName] = [cor];
			else {
				queued = messagesQueue[queueName].length > 0;
				messagesQueue[queueName].push(cor);
			}

			if (!queued)
				co(cor)();
		});
	};
	/* jshint ignore:end */


	this.onConnect = function*() {
		var ch = chan();

		var user = null;
		var filters = null;

		var authenticationRequired = function (messageHandler) {
			return function *(data, ch, cb){
				if (!user)
					throw new VerboseError('Access restricted!', 'access_restricted');

				yield messageHandler(data, ch, cb);
			};
		};

		var redisPrivateChannel = WebSocketClient.getPrivateRoomNameBySidCookie(socket.handshake.cookies['connect.sid'] ? socket.handshake.cookies['connect.sid'] : 'nothing');
		redis.subscribe(redisPrivateChannel);

		redis.on('message', (channel, message) => {
			if (channel == redisPrivateChannel)
			{
				if (message == 'auth-ok') {
					if (user)
						return;

					co(function *(){
						handshake.middleware(socket, ch);
						yield ch;

						session = socket.handshake.session;

						console.log('session reloaded...', session);

						yield checkAuth();
					})();
				}
				else
					socket.emit('auth-error');
			}
		});


		var checkAuth = function* () {
			var ch = chan();
			if (session.passport && session.passport.user)
			{
				db.User.findById(session.passport.user, ch);

				user = yield ch;

				console.log('checkAuth user is ', user);

				var filters;
				if (user.filters) {
					filters = JSON.parse(user.filters);
				}
				else
					filters = session.filters;

				if (session.favoriteSchools && session.favoriteSchools.length > 0)
					user.favoriteSchools = user.favoriteSchools.concat(session.favoriteSchools);

				if (user.filters) {
					filters.schools = filters.schools.filter(x => (x.id && x.id.toLowerCase() == 'national') || user.favoriteSchools.indexOf(mongoose.Types.ObjectId(x.id)) > -1);

					if (session.filters) {
						for (var school of session.filters.schools) {
							var i = filters.schools.findIndex(x => x.id == school.id);
							if (i < 0)
								filters.schools.push(school);
							else
								filters.schools[i].isSelected = school.isSelected;
						}
					}
				}

				// selection consistency check
				if (filters.schools.every(x => !x.isSelected))
					filters.schools = filters.schools.map(x => {
						if (x.id && x.id.toLowerCase() == 'national')
							x.isSelected = true;
						return x;
					});
				else if (filters.schools.length > 1 && filters.schools.every(x => x.isSelected))
					filters.schools = filters.schools.map(x => {
						if (x.id && x.id.toLowerCase() == 'national')
							x.isSelected = false;
						return x;
					});

				// update filters
				user.filters = JSON.stringify(filters);

				// clean up favoriteSchools for removed filters
				user.favoriteSchools = user.favoriteSchools.filter(x => filters.schools.findIndex(y =>
					y.id && y.id.toLowerCase() != 'national' && x.equals(mongoose.Types.ObjectId(y.id))) > -1);

				user.save(ch);
				yield ch;

				session.favoriteSchools = [];
				session.filters = null;
				session.save(ch);
				yield ch;

				if (user) {
					socket.emit('hello', {
						fullName: user.fullName
					});
					return true;
				}
			}

			return false;
		};

		var saveFilters = function* (filtersData) {
			var ch = chan();

			if (user)
			{
				user.filters = JSON.stringify(filtersData);
				user.save(ch);
				yield ch;
			}
			else
			{
				session.filters = filtersData;
				session.save(ch);
				yield ch;
			}
		};

		var setFilters = function* (dataFilters, isSave) {
			var ch = chan();

			if (isSave)
				yield saveFilters(dataFilters);

			filters = new Filters(dataFilters);
			filters.offset = 0;

			var list = yield filters.queryMongo(user ? user.inappropriateEventFiles : []);

			socket.emit('images/update', {
				isFull: true,
				for: filters.for,
				offset: filters.offset,
				list: list
			});
		};

		console.log('socket.io connected');
		socket.emit('connected', {
			app: {
				facebookAppId: config.FACEBOOK_APP_ID,
				recaptchaPublicKey: config.RECAPTCHA_PUBLIC_KEY
			}
		});

		if (isMobile)
		{
			try
			{
				var r = yield checkAuth();
				if (!r)
					socket.emit('hey');
			}
			catch(err)
			{
				console.error('socket.io on connection: exception during checking auth: ', err);
				return;
			}
		}
		else
			socket.on('ready', () => {
				co(function* (){
					try
					{
						var r = yield checkAuth();
						if (!r)
							socket.emit('hey');
					}
					catch(err)
					{
						console.error('socket.io on connection: exception during checking auth: ', err);
						return;
					}
				})();
			});

		/* DEFINE MESSAGE HANDLERS */
		onMessage('images', 'images/inappropriate-mark', authenticationRequired(function* (data, ch, cb) {
			var eventId = mongoose.Types.ObjectId(data.eventId);

			db.Event.findOne({_id: eventId, 's3list.name': data.photoName}, {'s3list.$': true}, ch);
			var event = yield ch;
			if (!event)
				return;

			var file = event.s3list[0];
			if (file.isInappropriateMark)
				return cb({isMarked: true});

			db.User.update({_id: user._id}, {$addToSet: {inappropriateEventFiles: file._id}}, ch);
			yield ch;

			mailgun.sendText(
				`Inappropriate Content <${config.CONTACT_EMAIL_FROM}>`,
				[config.CONTACT_EMAIL_TO],
				`${data.eventId} ${data.photoName}`,
				`Marked as inappropriate content by ${user.fullName}(${user._id})`,
				ch);
			yield ch;

			return cb({isMarked: true});
		}));

		onMessage('images/v-like', 'images/v-like', authenticationRequired(function* (data, ch, cb) {
			var eventId = mongoose.Types.ObjectId(data.eventId);

			var inc = 0;

			db.ImageStat.update(
				{eventId: eventId, name: data.name, $or: [
					{likedBy: {$exists: false}},
					{likedBy: {$nin: [user._id]}}
				]},
				{$inc: {likesCounter: 1}, $push: {likedBy: user._id}},
				{
					new: true,
					select: {
						likesCounter : true
					}
				},
				ch);
			var r = yield ch;

			if (r)
				inc = +1;
			else {
				db.ImageStat.update(
					{eventId: eventId, name: data.name, likedBy: {$in: [user._id]}},
					{$inc: {likesCounter: -1}, $pull: {likedBy: user._id}},
					{
						new: true,
						select: {
							likesCounter : true
						}
					},
					ch);
				r = yield ch;

				if (r)
					inc = -1;
			}

			if (inc !== 0) {
				db.Event.update(
					{_id: eventId, 's3list.name': data.name},
					{$inc: {'s3list.$.likesCounter': inc}},
					ch);
				yield ch;
			}

			db.Event.findOne(
				{_id: eventId, 's3list.name': data.name},
				{'s3list.$' : true},
				ch);
			var stat = yield ch;

			return cb({likes: stat.s3list[0].likesCounter});
		}));

		onMessage('imagesFiltersQueue', 'images/school-save', function* (data, ch, cb) {
			var schoolId = mongoose.Types.ObjectId(data.schoolId);

			if (!user) {
				session.favoriteSchools.push(schoolId);
				session.save(ch);
				yield ch;
				return;
			}

			db.User.update({_id: user._id}, {$addToSet: {
				favoriteSchools: schoolId
			}}, ch);
			yield ch;
		});

		onMessage('imagesFiltersQueue', 'images/filters-request', function* (data, ch) {
			var filtersData = user && user.filters ? JSON.parse(user.filters) : (session.filters ? session.filters : Filters.default);

			if (data.requirements)
			{
				filtersData.eventId = data.requirements.eventId ? data.requirements.eventId : undefined;

				filtersData.isFull = false;
				filtersData.for = data.requirements.kind;
				if (data.requirements.kind == 'ALBUMS') {
					filtersData.kind[0].isSelected = true;
					filtersData.kind[1].isSelected = false;
				}
				else
				if (data.requirements.kind == 'PHOTOS' || data.requirements.kind == 'ALBUM') {
					filtersData.kind[0].isSelected = false;
					filtersData.kind[1].isSelected = true;
				}
				else
				if (data.requirements.kind == 'DETAIL')
				{
					filtersData.kind[0].isSelected = false;
					filtersData.kind[1].isSelected = true;
					filtersData.isFull = true;
				}
			}

			console.log('images/filters-request use filters', filtersData);

			yield setFilters(filtersData, true);

			socket.emit('images/set-filters', {
				filters: filtersData
			});
		});

		onMessage('imagesFiltersQueue', 'images/filters-set', function* (data, ch) {
			yield setFilters(data.filters, 'isSave' in data ? data.isSave : true);
		});

		onMessage('imagesFiltersQueue', 'images/scroll', function* (data, ch) {
			if (!filters)
				return;

			filters.offset = data.offset;
			var list = yield filters.queryMongo(user ? user.inappropriateEventFiles : []);

			socket.emit('images/update', {
				isFull: false,
				for: filters.for,
				offset: filters.offset,
				list: list
			});
		});

		onMessage('images/comments', 'images/comments-request', function* (data, ch, cb) {
			var eventId = mongoose.Types.ObjectId(data.eventId);

			db.ImageStat
				.findOne({
					eventId: eventId,
					name: data.name
				}, {
					comments: true
				})
				.populate('comments.user', 'fullName')
				.exec(ch);
			var res = yield ch;

			if (!res)
				return;

			var comments = res.comments ? res.comments.map(x => {
				var days = Math.abs(moment(x.date).diff(moment(), 'days'));
				return {
					id: x._id,
					text: x.text,
					date: days < 1 ? 'today' : `${days}d`,
					fullName: x.user ? x.user.fullName : 'unknown',
					isMine: user && x.user == user._id
				};
			}).sort((a, b) => {
				a = new Date(a.date);
				b = new Date(a.date);
				return a < b ? -1 : (a == b ? 0 : 1);
			}) : [];

			cb({
				comments: comments
			});
		});

		onMessage('images/comments', 'images/add-comment', authenticationRequired(function* (data, ch, cb) {
			var eventId = mongoose.Types.ObjectId(data.eventId);
			var commentId = mongoose.Types.ObjectId();

			if (data.text)
				data.text = data.text.trim().substr(0, 140);
			if (!data.text)
				throw new VerboseError('no text specified', 'incorrect_input');

			db.ImageStat.update({
					eventId: eventId,
					name: data.name
				}, {
					$push: {
						comments: {
							_id: commentId,
							text: data.text,
							date: Date.now(),
							user: user._id
						}
					}
				}, {
					upsert: true
				},
				ch
			);
			var r = yield ch;

			cb({
				isOk: r == 1,
				commentId: commentId
			});
		}));

		onMessage('images/comments', 'images/remove-comment', authenticationRequired(function* (data, ch, cb) {
			var eventId = mongoose.Types.ObjectId(data.eventId);
			var commentId = mongoose.Types.ObjectId(data.commentId);

			db.ImageStat.update({
					eventId: eventId,
					name: data.name
				}, {
					$pull: {
						comments: {
							_id: commentId,
							user: user._id
						}
					}
				}, {
					upsert: true
				},
				ch
			);
			var r = yield ch;

			cb({isOk: r == 1});
		}));

		onMessage('school', 'search-school', function* (data, ch, cb) {
			if (!filters)
				return;

			if (data.id)
			{
				db.School.findById(data.id, ch);
			} else {
				db.School.findOne({
					$or: [
						{name: data.text},
						{longName: data.text}
					]
				}, ch);
			}

			var school = yield ch;

			if (!school)
				return cb({
					status: 'not-found'
				});

			if (filters.data.schools.filter(x => x.id == school._id).length > 0)
				return cb({status: 'already-exists'});

			cb({
				status: 'added',
				id: school._id,
				name: (school.name ? school.name : school.longName).trim()
			});
		});

		onMessage('school','typeahead-school', function* (data, ch, cb) {
			var query = {
				_source: [ 'name', 'longName', 'mascot.*', 'city', 'state' ],
				query: {
					/*fuzzy_like_this: {
						fields: ['longName'],
						like_text: data.text,
						max_query_terms: 12
					}*/
					/*wildcard: {
						longName: `${data.text}*`
					}*/
					query_string: {
						query: `${data.text}*`,
						fields: ['name', 'longName']
					}
				}
			};

			elasticSearchClient.search('idxschools', 'school', query, (err, data) => {
				if (err)
					return ch(err);
				ch(JSON.parse(data));
			});

			var res = yield ch;

			console.log(JSON.stringify(res, null, 2));

			cb({
				list: res.hits.hits.map(ee => {
					var e = ee._source;
					return {
						id: e._id,
						mascot: e.mascot.url,
						name: (e.name ? e.name : e.longName).trim(),
						location: `${e.city} ${e.state}`
					};
				})
			});
		});

		onMessage('contact-us', 'contact-us', function* (data, ch, cb) {
			var recaptchaAnswer = {
				remoteip: config.APP_LOC == 'local' ? socket.request.connection.remoteAddress : socket.request.headers["x-forwarded-for"],
				challenge: data.recaptcha.challenge,
				response: data.recaptcha.response
			};

			var recaptcha = new Recaptcha(config.RECAPTCHA_PUBLIC_KEY, config.RECAPTCHA_PRIVATE_KEY, recaptchaAnswer);
			recaptcha.verify((success, error_code) => ch(null, success ? true : error_code));
			var r = yield ch;

			if (r !== true)
				throw new VerboseError('wrong captcha, try again', 'recaptcha');

			mailgun.sendText(
				`Contact Us <${config.CONTACT_EMAIL_FROM}>`,
				[config.CONTACT_EMAIL_TO],
				data.form.subject,
				[data.form.text, `${data.form.name} <${data.form.email}>`].join('\n'),
				ch);
			yield ch;
		});

		onMessage('contact-us', 'contact-us/add-school', function* (data, ch, cb) {
			var recaptchaAnswer = {
				remoteip:  config.APP_LOC == 'local' ? socket.request.connection.remoteAddress : socket.request.headers["x-forwarded-for"],
				challenge: data.recaptcha.challenge,
				response:  data.recaptcha.response
			};

			var recaptcha = new Recaptcha(config.RECAPTCHA_PUBLIC_KEY, config.RECAPTCHA_PRIVATE_KEY, recaptchaAnswer);
			recaptcha.verify((success, error_code) => ch(null, success ? true : error_code));
			var r = yield ch;

			if (r !== true)
				throw new VerboseError('wrong captcha, try again', 'recaptcha');

			mailgun.sendText(
				`Add School <${config.CONTACT_EMAIL_FROM}>`,
				[config.CONTACT_EMAIL_TO],
				'Add School',
				[data.form.text, `${data.form.schoolName} ${data.form.schoolCity} ${data.form.schoolState}`, `${data.form.name} <${data.form.email}>`].join('\n'),
				ch);
			yield ch;
		});

		onMessage('auth', 'logout', function* (data, ch) {
			user = null;

			session.passport = null;
			session.filters = Filters.default;
			session.save(ch);
			yield ch;

			socket.emit('bye');
		});
	};
};

WebSocketClient.getPrivateRoomNameBySidCookie = (sidCookie) => `private-${crypto.createHash('sha256').update(sidCookie).digest('base64')}`;

module.exports = WebSocketClient;