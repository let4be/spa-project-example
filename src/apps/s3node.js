var
	chan				= require('chan'),
	co					= require('co'),
	sleep				= require('co-sleep'),
	AWS					= require('aws-sdk'),
	gm					= require('gm'),
	im					= gm.subClass({ imageMagick: true }),
	RedisClient			= require('../modules/redis'),
	S3Service			= require('../modules/s3Service');

var chc = (array) => {
	var ch = chan();
	array.push(ch);
	return ch;
};

var chcTolerant = (array) => {
	var ch = chan();
	array.push(ch);
	return (error, r) => {
		ch(null, {error: error, result: r});
	};
};

var ImageProcessor = function (s3) {
	var prepare = function *(body, name, isAutoOrient, isNormalize) {
		var ch = chan();

		var img = im(body, name);

		if (isAutoOrient)
			img.autoOrient();
		if (isNormalize)
			img.normalize();

		img
			.strip()
			.toBuffer(ch);

		return yield ch;
	};

	var resize = function *(body, name, width, maxHeight) {
		var ch = chan();

		var img = im(body, name);

		img
			.size((err, size) => {
				ch(err ? err : null, size);
			});

		var size = yield ch;

		var height = Math.floor(size.height * (width / size.width));
		if (height > maxHeight)
			width = Math.floor(width * (maxHeight / height));

		console.log(`resize ${name} to ${width}...`);

		img
			.scale(width)
			.toBuffer(ch);

		return yield ch;
	};

	var resizeThumb = function *(body, name, width, maxHeight) {
		var ch = chan();

		var img = im(body, name);

		img
			.size((err, size) => {
				ch(err ? err : null, size);
			});

		var size = yield ch;

		var height = Math.floor(size.height * (width / size.width));
		console.log(`resize ${name} to ${width}...`);

		img
			.scale(width)
			.crop(width, height > maxHeight ? maxHeight : height, 0, 0)
			.toBuffer(ch);

		return yield ch;
	};

	var createThumb = function *(body, name, width, maxHeight) {
		console.log(`Creating thumb(w${width} h max(${maxHeight}) for ${name}...`);

		return yield resizeThumb(body, name, width, maxHeight);
	};

	var createMobile = function *(body, name, width, maxHeight) {
		console.log(`Creating mobile(w${width} h max(${maxHeight}) for ${name}...`);

		return yield resize(body, name, width, maxHeight);
	};

	var createDetail = function *(body, name, width, maxHeight) {
		console.log(`Creating detail(w${width} h max(${maxHeight})) for ${name}...`);

		return yield resize(body, name, width, maxHeight);
	};

	this.process = function *(eventId, bucketName, key, isDetail, isMobile, isThumb, config) {
		var ch = chan();

		var highresKey = eventId + '/highres/' + key;
		var detailKey = eventId + '/detail/' + key;
		var mobileKey = eventId + '/mobile/' + key;
		var thumbKey = eventId + '/thumb/' + key;

		s3.get(bucketName, highresKey, ch);

		var obj = yield ch;

		var chans = [];

		var preparedBody = yield prepare(obj.Body, key, config.isAutoOrient, config.isNormalize);

		if (!isDetail) {
			var detailImage = yield createDetail(preparedBody, key, config.detailWidth, config.detailMaxHeight);

			console.log(`${detailKey} - uploading...`, Math.floor(detailImage.length / 1024) + 'kb');
			s3.put(bucketName, detailKey, detailImage, chcTolerant(chans));
		}

		if (!isMobile) {
			var mobileImage = yield createMobile(preparedBody, key, config.mobileWidth, config.mobileMaxHeight);

			console.log(`${mobileKey} - uploading...`, Math.floor(mobileImage.length / 1024) + 'kb');
			s3.put(bucketName, mobileKey, mobileImage, chcTolerant(chans));
		}

		if (!isThumb) {
			var thumbImage = yield createThumb(preparedBody, key, config.thumbWidth, config.thumbMaxHeight);

			console.log(`${thumbKey} - uploading...`, Math.floor(thumbImage.length / 1024) + 'kb');
			s3.put(bucketName, thumbKey, thumbImage, chcTolerant(chans));
		}

		var res = yield chans;
		console.log(`event '${eventId}' ${key} new images uploaded`, res);
	};
};

module.exports = function(config)
{
	var redis = RedisClient();

	AWS.config.update({accessKeyId: config.AWS_ACCESS_KEY_ID, secretAccessKey: config.AWS_SECRET_KEY, region: config.AWS_REGION});
	var s3 = new S3Service(new AWS.S3());
	var processor = new ImageProcessor(s3);
	var s3processorId = 0;

	co(function *() {
		var ch = chan();

		while (true) {
			redis.get('s3processor/id', ch);
			s3processorId = yield ch;
			if (s3processorId !== null)
				break;

			console.log('Wait for s3processor initialization...');
			yield sleep(500);
		}

		console.log('s3node ready... s3processorId is ', s3processorId);


		while (true) {
			try {
				redis.blpop(`${s3processorId}/s3node/new/task`, 0, ch);
				var task = JSON.parse((yield ch)[1]);

				console.log('got task', task);

				var startTime = new Date();
				var errorSaved = null;

				try {
					yield processor.process(task.eventId, task.bucketName, task.key, task.isDetail, task.isMobile, task.isThumb, task.config);
				} catch (error) {
					errorSaved = error;
					console.error(`${task.bucketName} ${task.key} - error`, error.stack);
				}
				console.log(`${task.bucketName} ${task.key} - processed`);

				redis.rpush(`${s3processorId}/s3node/${task.processingId}/result`, JSON.stringify({error: errorSaved, time: new Date() - startTime}));
			} catch (error) {
				console.error(`unhandled error while extracting job`, error.stack);
			}
		}
	})();
};
