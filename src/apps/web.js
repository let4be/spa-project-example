var
	path						= require('path'),
	mongoose					= require('mongoose'),
	moment						= require('moment'),
	http						= require('http'),
	request 					= require('request'),
	socketIo					= require('socket.io'),
	socketIoHandshake			= require('socket.io-handshake'),
	WebSocketServer				= require('ws').Server,
	express						= require('express'),
	expressBodyParser			= require('body-parser'),
	expressSession				= require('express-session'),
	ExpressRedisStore			= require('connect-redis')(expressSession),
	expressCookie				= require('cookie-parser'),
	staticGzip					= require('http-static-gzip-regexp'),
	chan						= require('chan'),
	co							= require('co'),
	sleep						= require('co-sleep'),

	RedisClient					= require('../modules/redis'),
	redis						= RedisClient(),

	ServerBridge				= require('../modules/serverBridge'),
	SocketIoAdaptor				= require('../modules/socketIoAdaptor'),

	WebSocketClient				= require('../modules/webSocketClient'),
	auth						= require('../modules/auth'),
	redisStore 					= new ExpressRedisStore({client: redis}),

	database					= require('../modules/database'),
	db 							= database.connect();

var classes = require('bl-classes'),
	VWEvent = classes.VWEvent,
	VWConfig = classes.VWConfig;

module.exports = function(config)
{
	var cookieSecret = '[deleted]';

	var app = express();

	app.use(expressBodyParser.json());
	app.use(expressBodyParser.urlencoded({
		extended: true
	}));
	app.use(expressCookie());
	app.use(expressSession({
		store: redisStore,
		secret: cookieSecret,
		maxAge: 360*5,
		proxy : true,
		saveUninitialized: true,
		resave: true
	}));

	app.use((req, res, next) => {
		req.session.initialized = true;
		return next();
	});

	auth(app, RedisClient());

	app.engine('jade', require('jade').__express);

	var viewsDir = path.join(__dirname, '..', 'www');
	console.log('views directory: ', viewsDir);

	app.set('views', viewsDir);
	app.set('view engine', 'jade');

	app.use( (req, res, next) => {
		var escapedFragment = req.query._escaped_fragment_;
		console.log('escapedFragment', escapedFragment);

		if (escapedFragment)
		{
			var args = escapedFragment.split('/').filter(x => !!x);
			if (args[0] == '#!')
				args.splice(0, 1);

			console.log('@', args.length, args[0], args[1]);
			if (args.length == 4 && args[0] == 'search' && args[1] == 'detail') {
				args = args.slice(-2);
				console.log('!', args);

				var eventId = args[0];
				var photoName = args[1];

				co(function *() {
					var ch = chan();

					console.log('escaped fragment for details detected', eventId, photoName);

					db.Event.findOne({
						_id: mongoose.Types.ObjectId(eventId),
						's3list.name': photoName
					}, {
						eventType: true,
						eventCustomTitle: true,
						sportType: true,
						sportLevel: true,
						gender: true,
						date: true,
						home: true,
						visitor: true
					}, ch);
					var event = yield ch;

					var ch1 = chan(), ch2 = chan();
					db.School.findById(event.home, ch1);
					db.School.findById(event.visitor, ch2);

					var schools = yield [ch1, ch2];
					console.log('@process crawler request for details view', event, schools);

					var cap = `${event.gender} ${event.sportLevel} ${event.sportType}`.toUpperCase();

					var vwEvent = new VWEvent({
						eventType: event.eventType,
						eventCustomTitle: event.eventCustomTitle,
						sportType: event.sportType,
						sportLevel: event.sportLevel,
						gender: event.gender,
						date: event.date,
						home: (schools[0].name ? schools[0].name : schools[0].longName).trim(),
						visitor: (schools[1].name ? schools[1].name : schools[1].longName).trim()
					});

					res.render('index', {
						isSocial: true,
						title: `Varsity Views - ${cap}`,
						description: `${vwEvent.title} ${moment(event.date).format('MMM DD, YYYY')}`,
						imageUrl: `${VWConfig.AMAZON_S3_BASE_URL}/${eventId}/detail/${photoName}`
					});
				})();

				return;
			}

			return next();
		}

		next();
	});

	app.use((req, res, next) => {
		console.log (`serve ${req.url}`);
		if (req.url == '/')
			req.url = '/index.html';
		req.url = req.url.replace(/\?.+$$/, '');
		console.log(req.url);
		return next();
	});

	app.use(staticGzip(/\.(js|css|html)/));
	app.use(express.static(config.WWW_ROOT));

	var server = http.Server(app);
	server.listen(config.WEB_PORT);
	var ServerBridgeInstance = ServerBridge(server);

	var redisForSocket = RedisClient();

	var handshake = socketIoHandshake({
		store: redisStore,
		key: 'connect.sid',
		secret: cookieSecret,
		parser: expressCookie()
	});

	var initializeSocketIoAPI = () => {
		var io = socketIo(new ServerBridgeInstance('web'));
		io.use(handshake);

		io.on('connection', (socket) => {
			try {
				var client = new WebSocketClient(socket, handshake, redisForSocket, config, false);

				co(function* () {
					try {
						yield client.onConnect();
					} catch (error) {
						console.error('error during Socket IO on connect: ', error.stack);
					}
				})();
			} catch (error) {
				console.error('error during Socket IO initialization: ', error.stack);
			}
		});
	};

	var initializeWebSocketAPI = () => {
		var wss = new WebSocketServer({server: new ServerBridgeInstance('mobile')});
		wss.on('connection', function (ws) {
			try {
				var socket = new SocketIoAdaptor(ws);

				co(function* () {
					var ch = chan();
					try {
						handshake.middleware(socket, ch);
						yield ch;

						var client = new WebSocketClient(socket, handshake, redisForSocket, config, true);
						yield client.onConnect();

						socket.ready();
					} catch (error) {
						console.error('error during Web Socket on connect: ', error.stack);
					}
				})();
			} catch (error) {
				console.error('error during Web Socket initialization: ', error.stack);
			}
		});
	};

	initializeSocketIoAPI();
	initializeWebSocketAPI();

	console.log(`HTTP Server is running at http://127.0.0.1:${config.WEB_PORT}/`);
	console.log(`root in ${config.WWW_ROOT}`);
};