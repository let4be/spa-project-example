var
	crypto				= require('crypto'),
	path				= require('path'),
	chan				= require('chan'),
	co					= require('co'),
	sleep				= require('co-sleep'),
	gather				= require('co-gather'),
	AWS					= require('aws-sdk'),
	fs					= require('fs'),
	gm					= require('gm'),
	im					= gm.subClass({ imageMagick: true }),
	mongoose			= require('mongoose'),
	Schema 				= mongoose.Schema,
	ObjectId			= Schema.ObjectId,
	RedisClient			= require('../modules/redis'),
	S3Service			= require('../modules/s3Service'),
	database			= require('../modules/database'),
	db 					= database.connect();

var chc = (array) => {
	var ch = chan();
	array.push(ch);
	return ch;
};

var chcTolerant = (array) => {
	var ch = chan();
	array.push(ch);
	return (error, r) => {
		ch(null, {error: error, result: r});
	};
};

var formatNameIndexes = (name) => {
	var r = /[0-9]+/g;
	var parts = path.basename(name, path.extname(name)).split(/[\-\_\.]/g);

	var sortableParts = [];
	for (var p of parts)
	{
		var pd;

		while ((pd = r.exec(p)))
			sortableParts.push(parseInt(pd, 10));
	}

	return sortableParts;
};

module.exports = function(config)
{
	var redis = RedisClient();

	AWS.config.update({accessKeyId: config.AWS_ACCESS_KEY_ID, secretAccessKey: config.AWS_SECRET_KEY, region: config.AWS_REGION});
	var s3 = new S3Service(new AWS.S3());
	var gConfig = null;

	var shared = {
		concurency: 0
	};
	var s3processorId = 0;

	var processEvent = function *(bucketName, eventId) {
		var ch = chan();
		var ch1 = chan();
		var s3 = new S3Service(new AWS.S3());

		console.log(`event '${eventId}' - checking for missing data buckets...`);

		var prefix = bucketName + '/' + eventId;
		/*s3.head(prefix + '/highres/', ch1);

		try {
			yield ch1;
		} catch (error)
		{
			console.error(`event '${eventId}' - error during accessing highres bucket ${prefix + '/highres/'}`, error.message, error.stack);
			return;
		}*/

		var statusQueries = [];
		s3.head(prefix + '/mobile/', chcTolerant(statusQueries));
		s3.head(prefix + '/detail/', chcTolerant(statusQueries));
		s3.head(prefix + '/thumb/', chcTolerant(statusQueries));
		var statuses = yield statusQueries;

		var createQueries = [];
		if (statuses[0].error && statuses[0].error.code == 'NotFound')
			s3.create(prefix + '/mobile/', chc(createQueries));
		if (statuses[1].error && statuses[1].error.code == 'NotFound')
			s3.create(prefix + '/detail/', chc(createQueries));
		if (statuses[2].error && statuses[2].error.code == 'NotFound')
			s3.create(prefix + '/thumb/', chc(createQueries));

		statuses = [];
		if (createQueries.length > 0)
			try {
				statuses = yield createQueries;
			} catch (error)
			{
				console.error(`event '${eventId}' - cannot create missing data buckets`, error.stack);
				return;
			}

		console.log(`event '${eventId}' - ready for processing`, statuses);

		console.log(`event '${eventId}' - processing...`);

		try {
			db.Event.findOne({
				_id: eventId,
				albumStatus: 'live'
			}, {
				s3list: true
			}, ch);
			var dbEvent = yield ch;

			if (!dbEvent) {
				console.warn(`event '${eventId}' - skipping no matching mongo record found!`);
				return;
			}

			var s3list = dbEvent.s3list && dbEvent.s3list.length > 0 ? dbEvent.s3list.reduce((t, c) => {
				t[c.name.toUpperCase()] = c;
				return t;
			}, {}) : {};

			db.S3Config.findOne({}, {_id: false, __v: false}, ch);
			var s3config = yield ch;
			if (!s3config) {
				console.warn(`event '${eventId}' - skipping no config found!`);
				return;
			}
			var s3configHash = crypto.createHash('sha256').update(JSON.stringify(s3config)).digest('base64');

			var channels = [];
			s3.listPhotos(bucketName, eventId, 'highres', chc(channels));
			s3.listPhotos(bucketName, eventId, 'detail', chc(channels));
			s3.listPhotos(bucketName, eventId, 'mobile', chc(channels));
			s3.listPhotos(bucketName, eventId, 'thumb', chc(channels));
			var objects = yield channels;

			var highresObjects = objects[0];
			var detailObjects = objects[1];
			var mobileObjects = objects[2];
			var thumbObjects = objects[3];

			var highresObjectsByKey = s3.transformList(highresObjects);
			var detailObjectsByKey = s3.transformList(detailObjects);
			var mobileObjectsByKey = s3.transformList(mobileObjects);
			var thumbObjectsByKey = s3.transformList(thumbObjects);

			var queuedCount = 0;

			var process = function *(key, isDetail, isMobile, isThumb) {
				var ch = chan();

				try {
					redis.incr(`${s3processorId}/s3node/id`, ch);
					var id = yield ch;

					db.Event.update({
						_id: eventId,
						's3list.name': key
					}, {
						$set: {
							's3list.$.isProcessing': true
						}
					}, ch);
					var r = yield ch;

					if (!r) {
						db.Event.update({
							_id: eventId,
							's3list.name': {$ne: key}
						}, {
							$push: {'s3list': {
								name: key,
								isProcessing: true,
								isInappropriateMark: false,
								viewsCounter: 0,
								likesCounter: 0
							}}
						}, ch);
						yield ch;
					}

					db.ImageStat.findOne({
						eventId: eventId,
						name: key
					}, ch);
					var imageStat = yield ch;
					if (!imageStat) {
						imageStat = new db.ImageStat({
							eventId: eventId,
							name: key
						});
						imageStat.save(ch);
						yield ch;
					}

					console.log(`${key} - queueing as ${id}...`);

					redis.rpush(`${s3processorId}/s3node/new/task`, JSON.stringify({
						processingId: id,
						bucketName: bucketName,
						eventId: eventId,
						key: key,
						isDetail: isDetail,
						isMobile: isMobile,
						isThumb: isThumb,
						config: s3config
					}), ch);
					yield ch;

					redis.blpop(`${s3processorId}/s3node/${id}/result`, 180, ch);
					var result = yield ch;
					console.log('result', result);

					if (result) {
						result = JSON.parse(result[1]);

						imageStat.errorMessage = result.error ? result.error.message : null;
						imageStat.errorStack = result.error ? result.error.stack : null;
						imageStat.processedDate = Date.now();
						imageStat.processingTimeMs = result.time;
						imageStat.save(ch);
						yield ch;

						if (!result.error) {
							var i = 0;
							var fields = {};
							var nameIndexes = formatNameIndexes(key);
							for(var ni of nameIndexes) {
								i++;
								fields[`s3list.$.nameIndex${i}`] = ni;
							}
							fields['s3list.$.isProcessing'] = false;
							fields['s3list.$.s3ConfigHash'] = s3configHash;


							db.Event.update({
								_id: eventId,
								's3list.name': key
							}, {
								$set: fields
							}, ch);
							yield ch;
						}
					} else {
						imageStat.errorMessage = 'operation timeout';
						imageStat.save(ch);
						yield ch;
					}
				} catch (error) {
					console.error(`event '${eventId}' processing error`, error.code, error.stack);
				} finally {
					shared.concurency--;
				}
			};

			console.log(`process event '${eventId}'...`);
			for(var hrObjKey in highresObjectsByKey)
				if (highresObjectsByKey.hasOwnProperty(hrObjKey)) {
					var realHrObjKey = path.basename(highresObjectsByKey[hrObjKey].Key);
					var isDetail = hrObjKey in detailObjectsByKey;
					var isMobile = hrObjKey in mobileObjectsByKey;
					var isThumb = hrObjKey in thumbObjectsByKey;

					if (!s3list[hrObjKey] || s3list[hrObjKey].s3ConfigHash != s3configHash) {
						console.log(
							`event '${eventId}' '${hrObjKey}' config hash mismatch - reprocess`,
							s3list[hrObjKey] && s3list[hrObjKey].s3ConfigHash ? s3list[hrObjKey].s3ConfigHash : '[empty]',
							s3configHash ? s3configHash : '[empty]');
						isDetail = false;
						isMobile = false;
						isThumb = false;
					}

					if (!isDetail || !isMobile || !isThumb) {
						while (shared.concurency >= gConfig.s3Concurency)
						{
							yield sleep(100);
						}

						((realHrObjKey, isDetail, isMobile, isThumb) => {
							queuedCount++;
							shared.concurency++;
							co(process)(realHrObjKey, isDetail, isMobile, isThumb);
						})(realHrObjKey, isDetail, isMobile, isThumb);
					}
				}

			console.log(`event '${eventId}' ${queuedCount} item(s) queued for processing...`);

			if (queuedCount > 0) {
				redis.keys('/web/images/*', ch);
				var keys = yield ch;

				var multi = redis.multi();
				for(var key of keys)
					multi.del(key);
				multi.exec(ch);
				yield ch;
			}
		} catch (error) {
			console.error(`event '${eventId}' - fatal error`, error.code, error.stack);
		}
	};

	co(function*(){
		var bucketName = 'vv-prod-images';

		try {
			var ch = chan();

			console.log('reading configs...');

			redis.incr('s3processor/id', ch);
			s3processorId = yield ch;

			while (true) {
				db.S3Config.findOne({}, ch);
				var s3config = yield ch;
				if (!s3config) {
					s3config = new db.S3Config({});
					s3config.save(ch);
					yield ch;
				}

				db.Config.findOne({}, ch);
				gConfig = yield ch;
				if (!gConfig) {
					gConfig = new db.Config({});
					gConfig.save(ch);
					yield ch;
				}

				console.log(`isReprocessImagesOnS3ConfigChange: ${gConfig.isReprocessImagesOnS3ConfigChange}, s3Concurency: ${gConfig.s3Concurency}`);

				console.log(`Checking s3 bucket '${bucketName}'...`);

				try {
					s3.head(bucketName, ch);
					var bucketStatus = yield ch;

					console.log(`Bucket '${bucketName}' ok`, bucketStatus);

					s3.listEvents(bucketName, ch);
					var eventObjects = yield ch;

					for (var eventObj of eventObjects.CommonPrefixes) {
						var eventId = eventObj.Prefix.split('/')[0];
						if (mongoose.Types.ObjectId.isValid(eventId))
							yield processEvent(bucketName, eventId);
						else
							console.warn(`events processing: skip event ${eventId} - not valid event identificator`);
					}
				} catch (error) {
					console.error('events processing - unhandled error', error.code, error.stack);
				}

				console.log('all tasks queued for processing, await...');
				while (shared.concurency > 0) {
					yield sleep(1000);
					console.log(`all tasks queued for processing, current concurency: ${shared.concurency}... await...`);
				}

				console.log('events processing finished - snap 60 sec...');
				yield sleep(60000);
			}
		} catch(err) {
			console.error(err.code, err.stack);
		}
	})();
};
