var
	chan				= require('chan'),
	co					= require('co'),
	sleep				= require('co-sleep'),
	path				= require('path'),
	mongoose			= require('mongoose'),
	database			= require('../modules/database'),
	db 					= database.connect();


module.exports = function(config)
{
	co(function *(){
		var ch = chan();

		db.Event.find({}, {_id: true, date: true, home: true, visitor: true, s3list: true}, ch);
		var events = yield ch;

		var fix = (oid) => oid.replace(['objectid', '(', ')'], '');
		var formatNameIndexes = (name) => {
			var r = /[0-9]+/g;
			var parts = path.basename(name, path.extname(name)).split(/[\-\_\.]/g);

			var sortableParts = [];
			for (var p of parts)
			{
				var pd;

				while ((pd = r.exec(p)))
					sortableParts.push(parseInt(pd, 10));
			}

			return sortableParts;
		};

		console.log(`found ${events.length} event(s)...`);

		var chans = [];
		for(var event of events) {
			var chn = chan();

			if (event.home)
				event.home = fix(event.home.toString().toLowerCase());

			if (event.visitor)
				event.visitor = fix(event.visitor.toString().toLowerCase());

			console.log(`processing eventId ${event._id}...`);

			db.Event.collection.update({_id: event._id}, {$set: {
				home: mongoose.Types.ObjectId(event.home),
				visitor: mongoose.Types.ObjectId(event.visitor),
				date: new Date(event.date.toString())
			}}, chn);
			chans.push(chn);

			for(var s3e of event.s3list)
			{
				var chn1 = chan();

				var nameIndexes = formatNameIndexes(s3e.name);
				var i = 0;
				var fields = {};
				for(var ni of nameIndexes)
				{
					i++;
					fields[`s3list.$.nameIndex${i}`] = ni;
				}

				db.Event.collection.update({
					_id: event._id, 's3list.name': s3e.name
				}, {
					$set: fields
				}, chn1);
				chans.push(chn1);
			}
		}

		if (chans.length > 0) {
			console.log(`processing ${chans.length} requests...`);

			yield chans;

			console.log('done!');
		}

		console.log('nothing to do!');
	})();
};

