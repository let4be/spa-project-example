var
	traceur = require('gulp-traceur'), // somehow this shit needs to be included 1st

	fs = require('fs'),
	path = require('path'),
	es = require('event-stream'),
	spawn = require('child_process').spawn,

	through = require('through'),
	traceurify = require('traceurify'),
	ngminify = require('browserify-ngmin'),
	bulkify = require('bulkify'),
	debowerify = require('debowerify'),
	deamdify = require('deamdify'),
	filterTransform = require('filter-transform'),
	templateCache = require('gulp-angular-templatecache'),
	gulp = require('gulp'),
	plugins = require('gulp-load-plugins')({
		pattern: ['gulp-*', 'gulp.*'],
		replaceString: /\bgulp[\-.]/
	});

var thisPackage = JSON.parse(fs.readFileSync('package.json'));

var config = {
	// path settings
	srcServerJs: ['src/*.js', 'src/apps/**/*.js', 'src/modules/**/*.js', 'src/node_modules/**/*.js'],
	srcServerJsBase: 'src',
	srcClientJs: ['src/www/js/*.js', 'src/www/js/**/*.js', 'src/node_modules/**/*.js'],
	srcClientAngularJs: ['src/www/js/*.js', 'src/www/js/**/*.js'],
	srcClientJsEntry: 'src/www/js/main.js',
	srcBootstrapCss: 'node_modules/bootstrap/dist/css/bootstrap.css',
	srcBootstrapJs: 'node_modules/bootstrap/dist/js/bootstrap.js',
	srcClientHtml: ['src/www/**/*.html'],
	srcClientJade: ['src/www/index.jade'],
	srcClientJadePages: ['src/www/pages/**/*.jade'],
	srcClientLess: ['src/www/less/**/*.less'],
	srcClientImages: 'src/www/images/**/*',
	srcClientFonts: ['src/www/fonts/*'],
	dstClientHtml: 'public/www',
	dstClientHtmlIndex: 'public/www/index.html',
	dstClientHtmlPages: 'public/www/pages',
	dstClientJs: 'public/www/js',
	dstClientCss: 'public/www/css',
	dstClientImages: 'public/www/images',
	dstClientFonts: 'public/www/fonts',
	dstServerJs: 'public',

	// conditional settings
	isLint: true,
	isCompressImages: true,
	isMinify: true,
	isUglify: true
};

var onError = function(err) {
	plugins.util.log(err instanceof plugins.util.PluginError ? plugins.util.colors.red(err.message) : err.stack);
	plugins.util.beep();
};

var plumb = function() {
	return plugins.plumber({
		errorHandler: onError
	});
};

gulp.task('server', ['node-js'], function() {
	var kill = spawn('killall', ['node']);
	kill.on('close', function() {
		var server = spawn('foreman', ['start'], {
			cwd: process.cwd()
		});

		server.stdout.setEncoding('utf8');
		server.stdout.on('data', function(data) {
			plugins.util.log(data);
		});

		server.stderr.setEncoding('utf8');
		server.stderr.on('data', function(data) {
			plugins.util.log(plugins.util.colors.red(data));
		});
	});
});

gulp.task('clean-dst', function() {
	return gulp.src('./public/*', {
		read: false
	})
		.pipe(plumb())
		.pipe(plugins.clean());
});

gulp.task('clean', ['clean-dst'], function() {
	fs.mkdirSync('./public/modules');
	fs.mkdirSync('./public/www');
	fs.mkdirSync('./public/www/js');
	fs.mkdirSync('./public/www/images');
	fs.mkdirSync('./public/www/css');
});

gulp.task('images', function() {
	return gulp.src(config.srcClientImages)
		.pipe(plumb())
		.pipe(plugins.watch(function(files) {
			return files
				.pipe(plugins.if(config.isCompressImages, plugins.imagemin({
					progressive: true
				})))
				.pipe(gulp.dest(config.dstClientImages))
				.pipe(plugins.livereload());
		}));
});

gulp.task('fonts', function() {
	return gulp.src(config.srcClientFonts)
		.pipe(gulp.dest(config.dstClientFonts))
		.pipe(plugins.livereload());
});

gulp.task('jade-pages', function() {
	return gulp.src(config.srcClientJadePages)
		.pipe(plumb())
		.pipe(plugins.jade({
			locals: {
				version: thisPackage.version,
				name: thisPackage.name,
				description: thisPackage.description
			},
			doctype: 'html'
		}))
		.pipe(plugins.if(config.isMinify, plugins.minifyHtml({
			empty: true
		})))
		.pipe(templateCache({
			module: 'NgApplication',
			root: 'pages/'
		}))
		.pipe(plugins.rename('templates.min.js'))
		.pipe(gulp.dest(config.dstClientJs))
		.pipe(plugins.gzip())
		.pipe(gulp.dest(config.dstClientJs))
		.pipe(plugins.livereload());
});

gulp.task('jade', ['jade-pages'], function() {
	return gulp.src(config.srcClientJade)
		.pipe(plumb())
		.pipe(gulp.dest(config.dstClientHtml))
		.pipe(plugins.jade({
			locals: {
				version: thisPackage.version,
				name: thisPackage.name,
				description: thisPackage.description
			},
			doctype: 'html'
		}))
		.pipe(plugins.if(config.isMinify, plugins.minifyHtml({
			empty: true,
			comments: true
		})))
		.pipe(gulp.dest(config.dstClientHtml))
		.pipe(plugins.gzip())
		.pipe(gulp.dest(config.dstClientHtml))
		.pipe(plugins.livereload());
});

/*gulp.task('inline', ['jade'], function() {
	return gulp.src(config.dstClientHtmlIndex)
		.pipe(plumb())
		.pipe(plugins.smoosher())
		.pipe(gulp.dest(config.dstClientHtml))
		.pipe(plugins.gzip())
		.pipe(gulp.dest(config.dstClientHtml))
		.pipe(plugins.livereload());
});*/

gulp.task('html', function() {
	return gulp.src(config.srcClientHtml)
		.pipe(plumb())
		.pipe(plugins.if(config.isMinify, plugins.minifyHtml()))
		.pipe(gulp.dest(config.dstClientHtml))
		.pipe(plugins.gzip())
		.pipe(gulp.dest(config.dstClientHtml))
		.pipe(plugins.livereload());
});

gulp.task('less', function() {
	return es.concat(
		gulp.src(config.srcBootstrapCss)
			.pipe(plumb()),
		gulp.src(config.srcClientLess)
			.pipe(plumb())
			.pipe(plugins.less({
				paths: [path.join(__dirname, 'src', 'www', 'less', 'include')]
			}))
			.pipe(plugins.colorguard())
	)
		.pipe(plugins.concat('app.css'))
		/*.pipe(plugins.if(config.isMinify, plugins.uncss({
			html: fs.readdirSync('./public/www/').concat(fs.readdirSync('./public/www/typeahead/')),
			htmlroot: path.join(__dirname, 'public', 'src', 'www')
		 })))*/
		.pipe(gulp.dest(config.dstClientCss))
		.pipe(plugins.if(config.isMinify, plugins.minifyCss({
			keepSpecialComments: 0
		})))
		.pipe(plugins.rename('app.min.css'))
		.pipe(plugins.size())
		.pipe(gulp.dest(config.dstClientCss))
		.pipe(plugins.gzip())
		.pipe(gulp.dest(config.dstClientCss))
		.pipe(plugins.livereload());
});

gulp.task('node-js', function() {
	var filter = plugins.filter(function (f) {
		return !f.path.contains('src/node_modules');
	});

	return gulp.src(config.srcServerJs, {
		base: config.srcServerJsBase
	})
		.pipe(plumb())
		.pipe(filter)
		.pipe(plugins.if(config.isLint, plugins.jshint({
			esnext: true,
			noyield: true
		})))
		.pipe(plugins.if(config.isLint, plugins.jshint.reporter('jshint-stylish')))
		.pipe(plugins.if(config.isLint, plugins.jshint.reporter('fail')))
		.pipe(filter.restore())
		.pipe(traceur({
			//generators: 'parse'
		}))
		.pipe(gulp.dest(config.dstServerJs));
});

gulp.task('client-validate-js', function(){
	var filter = plugins.filter(function (f) {
		return !f.path.contains('src/node_modules');
	});

	return gulp.src(config.srcClientJs)
		.pipe(plumb())
		.pipe(filter)
		.pipe(plugins.ignore.exclude(/\/vendor\//))
		.pipe(plugins.if(config.isLint, plugins.jshint({
			esnext: true,
			noyield: true
		 })))
		 .pipe(plugins.if(config.isLint, plugins.jshint.reporter('jshint-stylish')))
		 .pipe(plugins.if(config.isLint, plugins.jshint.reporter('fail')));
});

gulp.task('client-js', ['client-validate-js'], function() {
	return gulp.src(config.srcClientJsEntry, { read: false })
		.pipe(plumb())
		.pipe(plugins.browserify({
			transform: [
				[{global: true}, filterTransform(
					function(file) {
						if (file.indexOf('/git/src/') < 0 || file.indexOf('/git/src/www/js/vendor/') > -1)
							return false;

						console.log('Traceur ES6 -> ES5: ' + file);
						return true;
					},
					traceurify({
						//blockBinding: true
					}))
				],
				[{global: true}, filterTransform(
					function(file) {
						if (file.indexOf('/git/src/www/js/') < 0 || file.indexOf('/git/src/www/js/vendor/') > -1)
							return false;

						console.log('Angular prepare: ' + file);
						return true;
					},
					ngminify)
				],
				[{global: true}, filterTransform(
					function(file) {
						if (file.indexOf('/git/src/www/js/') < 0 || file.indexOf('/git/src/www/js/vendor/') > -1)
							return false;

						console.log('Bulkify prepare: ' + file);
						return true;
					},
					bulkify)
				],
				[{global: true}, filterTransform(
					function(file) {
						if (file.indexOf('/git/src/www/js/') < 0 || file.indexOf('/git/src/www/js/vendor/') > -1)
							return false;

						console.log('Debowerify prepare: ' + file);
						return true;
					},
					debowerify)
				],
				[{global: true}, filterTransform(
					function(file) {
						if (file.indexOf('/git/src/www/js/') < 0 || file.indexOf('/git/src/www/js/vendor/') > -1)
							return false;

						console.log('Deamdify prepare: ' + file);
						return true;
					},
					deamdify)
				]
			],
			extensions: ['.js'],
			//insertGlobals : true,
			debug: true
		}))
		.pipe(plugins.rename('app.min.js'))
		.pipe(plugins.sourcemaps.init({loadMaps: true}))
		.pipe(plugins.if(config.isUglify, plugins.uglify()))
		.pipe(plugins.sourcemaps.write('./'))
		.pipe(gulp.dest(config.dstClientJs))
		.pipe(plugins.ignore.exclude(/.*app\.min\.js\.map$/))
		.pipe(plugins.gzip())
		.pipe(gulp.dest(config.dstClientJs))
		.pipe(plugins.livereload());
});

gulp.task('watch', function() {
	plugins.livereload.listen();
	gulp.watch(config.srcServerJs, ['server']);
	gulp.watch(config.srcClientJs, ['client-js']);
	gulp.watch(config.srcClientHtml, ['html']);
	gulp.watch(config.srcClientJadePages, ['jade-pages']);
	gulp.watch(config.srcClientJade, ['jade']);
	gulp.watch(config.srcClientLess, ['less']);
	gulp.watch(config.srcClientImages, ['images']);
	gulp.watch(config.srcClientFonts, ['fonts']);
});

gulp.task('default', ['node-js', 'client-js', 'jade', 'html', 'less', 'images', 'fonts', 'server', 'watch']);